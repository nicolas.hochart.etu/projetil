/**
 */
package adl.tests;

import adl.ApplicationLogicielle;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Application Logicielle</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ApplicationLogicielleTest extends TestCase {

	/**
	 * The fixture for this Application Logicielle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationLogicielle fixture = null;

	/**
	 * Constructs a new Application Logicielle test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationLogicielleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Application Logicielle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ApplicationLogicielle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Application Logicielle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationLogicielle getFixture() {
		return fixture;
	}

} //ApplicationLogicielleTest
