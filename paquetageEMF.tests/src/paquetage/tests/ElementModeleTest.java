/**
 */
package paquetage.tests;

import junit.framework.TestCase;

import paquetage.ElementModele;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Element Modele</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ElementModeleTest extends TestCase {

	/**
	 * The fixture for this Element Modele test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementModele fixture = null;

	/**
	 * Constructs a new Element Modele test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementModeleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Element Modele test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ElementModele fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Element Modele test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementModele getFixture() {
		return fixture;
	}

} //ElementModeleTest
