/**
 */
package paquetage.tests;

import junit.textui.TestRunner;

import paquetage.Classe;
import paquetage.PaquetageFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Classe</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClasseTest extends ElementModeleTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClasseTest.class);
	}

	/**
	 * Constructs a new Classe test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClasseTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Classe test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Classe getFixture() {
		return (Classe)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(PaquetageFactory.eINSTANCE.createClasse());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClasseTest
