/**
 */
package paquetage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Logicielle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.ApplicationLogicielle#getNom <em>Nom</em>}</li>
 *   <li>{@link paquetage.ApplicationLogicielle#getSimple <em>Simple</em>}</li>
 *   <li>{@link paquetage.ApplicationLogicielle#getComplexe <em>Complexe</em>}</li>
 *   <li>{@link paquetage.ApplicationLogicielle#getRequiert <em>Requiert</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getApplicationLogicielle()
 * @model
 * @generated
 */
public interface ApplicationLogicielle extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see paquetage.PaquetagePackage#getApplicationLogicielle_Nom()
	 * @model
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link paquetage.ApplicationLogicielle#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple</em>' reference.
	 * @see #setSimple(Simple)
	 * @see paquetage.PaquetagePackage#getApplicationLogicielle_Simple()
	 * @model
	 * @generated
	 */
	Simple getSimple();

	/**
	 * Sets the value of the '{@link paquetage.ApplicationLogicielle#getSimple <em>Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple</em>' reference.
	 * @see #getSimple()
	 * @generated
	 */
	void setSimple(Simple value);

	/**
	 * Returns the value of the '<em><b>Complexe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complexe</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complexe</em>' reference.
	 * @see #setComplexe(Complexe)
	 * @see paquetage.PaquetagePackage#getApplicationLogicielle_Complexe()
	 * @model
	 * @generated
	 */
	Complexe getComplexe();

	/**
	 * Sets the value of the '{@link paquetage.ApplicationLogicielle#getComplexe <em>Complexe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Complexe</em>' reference.
	 * @see #getComplexe()
	 * @generated
	 */
	void setComplexe(Complexe value);

	/**
	 * Returns the value of the '<em><b>Requiert</b></em>' reference list.
	 * The list contents are of type {@link paquetage.Requiert}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requiert</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requiert</em>' reference list.
	 * @see paquetage.PaquetagePackage#getApplicationLogicielle_Requiert()
	 * @model
	 * @generated
	 */
	EList<Requiert> getRequiert();

} // ApplicationLogicielle
