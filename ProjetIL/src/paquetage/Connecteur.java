/**
 */
package paquetage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.Connecteur#getNumero <em>Numero</em>}</li>
 *   <li>{@link paquetage.Connecteur#getPortentrée <em>Portentrée</em>}</li>
 *   <li>{@link paquetage.Connecteur#getPortsortie <em>Portsortie</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getConnecteur()
 * @model
 * @generated
 */
public interface Connecteur extends EObject {
	/**
	 * Returns the value of the '<em><b>Numero</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Numero</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numero</em>' attribute.
	 * @see #setNumero(int)
	 * @see paquetage.PaquetagePackage#getConnecteur_Numero()
	 * @model required="true"
	 * @generated
	 */
	int getNumero();

	/**
	 * Sets the value of the '{@link paquetage.Connecteur#getNumero <em>Numero</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Numero</em>' attribute.
	 * @see #getNumero()
	 * @generated
	 */
	void setNumero(int value);

	/**
	 * Returns the value of the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portentrée</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portentrée</em>' reference.
	 * @see #setPortentrée(PortEntrée)
	 * @see paquetage.PaquetagePackage#getConnecteur_Portentrée()
	 * @model
	 * @generated
	 */
	PortEntrée getPortentrée();

	/**
	 * Sets the value of the '{@link paquetage.Connecteur#getPortentrée <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portentrée</em>' reference.
	 * @see #getPortentrée()
	 * @generated
	 */
	void setPortentrée(PortEntrée value);

	/**
	 * Returns the value of the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portsortie</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portsortie</em>' reference.
	 * @see #setPortsortie(PortSortie)
	 * @see paquetage.PaquetagePackage#getConnecteur_Portsortie()
	 * @model
	 * @generated
	 */
	PortSortie getPortsortie();

	/**
	 * Sets the value of the '{@link paquetage.Connecteur#getPortsortie <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portsortie</em>' reference.
	 * @see #getPortsortie()
	 * @generated
	 */
	void setPortsortie(PortSortie value);

} // Connecteur
