/**
 */
package paquetage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complexe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.Complexe#getPartieapplication <em>Partieapplication</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getComplexe()
 * @model
 * @generated
 */
public interface Complexe extends EObject {
	/**
	 * Returns the value of the '<em><b>Partieapplication</b></em>' reference list.
	 * The list contents are of type {@link paquetage.PartieApplication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partieapplication</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partieapplication</em>' reference list.
	 * @see paquetage.PaquetagePackage#getComplexe_Partieapplication()
	 * @model
	 * @generated
	 */
	EList<PartieApplication> getPartieapplication();

} // Complexe
