/**
 */
package paquetage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import paquetage.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PaquetageFactoryImpl extends EFactoryImpl implements PaquetageFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PaquetageFactory init() {
		try {
			PaquetageFactory thePaquetageFactory = (PaquetageFactory)EPackage.Registry.INSTANCE.getEFactory(PaquetagePackage.eNS_URI);
			if (thePaquetageFactory != null) {
				return thePaquetageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PaquetageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaquetageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PaquetagePackage.APPLICATION_LOGICIELLE: return createApplicationLogicielle();
			case PaquetagePackage.PARTIE_APPLICATION: return createPartieApplication();
			case PaquetagePackage.PORT_ENTRÉE: return createPortEntrée();
			case PaquetagePackage.PORT_SORTIE: return createPortSortie();
			case PaquetagePackage.CONNECTEUR: return createConnecteur();
			case PaquetagePackage.RESSOURCE: return createRessource();
			case PaquetagePackage.SIMPLE: return createSimple();
			case PaquetagePackage.COMPLEXE: return createComplexe();
			case PaquetagePackage.REQUIERT: return createRequiert();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationLogicielle createApplicationLogicielle() {
		ApplicationLogicielleImpl applicationLogicielle = new ApplicationLogicielleImpl();
		return applicationLogicielle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartieApplication createPartieApplication() {
		PartieApplicationImpl partieApplication = new PartieApplicationImpl();
		return partieApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée createPortEntrée() {
		PortEntréeImpl portEntrée = new PortEntréeImpl();
		return portEntrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie createPortSortie() {
		PortSortieImpl portSortie = new PortSortieImpl();
		return portSortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur createConnecteur() {
		ConnecteurImpl connecteur = new ConnecteurImpl();
		return connecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ressource createRessource() {
		RessourceImpl ressource = new RessourceImpl();
		return ressource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple createSimple() {
		SimpleImpl simple = new SimpleImpl();
		return simple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Complexe createComplexe() {
		ComplexeImpl complexe = new ComplexeImpl();
		return complexe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requiert createRequiert() {
		RequiertImpl requiert = new RequiertImpl();
		return requiert;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaquetagePackage getPaquetagePackage() {
		return (PaquetagePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PaquetagePackage getPackage() {
		return PaquetagePackage.eINSTANCE;
	}

} //PaquetageFactoryImpl
