/**
 */
package paquetage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import paquetage.ApplicationLogicielle;
import paquetage.Complexe;
import paquetage.PaquetagePackage;
import paquetage.Requiert;
import paquetage.Simple;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Application Logicielle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link paquetage.impl.ApplicationLogicielleImpl#getNom <em>Nom</em>}</li>
 *   <li>{@link paquetage.impl.ApplicationLogicielleImpl#getSimple <em>Simple</em>}</li>
 *   <li>{@link paquetage.impl.ApplicationLogicielleImpl#getComplexe <em>Complexe</em>}</li>
 *   <li>{@link paquetage.impl.ApplicationLogicielleImpl#getRequiert <em>Requiert</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApplicationLogicielleImpl extends MinimalEObjectImpl.Container implements ApplicationLogicielle {
	/**
	 * The default value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected static final String NOM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected String nom = NOM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSimple() <em>Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimple()
	 * @generated
	 * @ordered
	 */
	protected Simple simple;

	/**
	 * The cached value of the '{@link #getComplexe() <em>Complexe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComplexe()
	 * @generated
	 * @ordered
	 */
	protected Complexe complexe;

	/**
	 * The cached value of the '{@link #getRequiert() <em>Requiert</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiert()
	 * @generated
	 * @ordered
	 */
	protected EList<Requiert> requiert;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationLogicielleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PaquetagePackage.Literals.APPLICATION_LOGICIELLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNom(String newNom) {
		String oldNom = nom;
		nom = newNom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.APPLICATION_LOGICIELLE__NOM, oldNom, nom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple getSimple() {
		if (simple != null && simple.eIsProxy()) {
			InternalEObject oldSimple = (InternalEObject)simple;
			simple = (Simple)eResolveProxy(oldSimple);
			if (simple != oldSimple) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE, oldSimple, simple));
			}
		}
		return simple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple basicGetSimple() {
		return simple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimple(Simple newSimple) {
		Simple oldSimple = simple;
		simple = newSimple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE, oldSimple, simple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Complexe getComplexe() {
		if (complexe != null && complexe.eIsProxy()) {
			InternalEObject oldComplexe = (InternalEObject)complexe;
			complexe = (Complexe)eResolveProxy(oldComplexe);
			if (complexe != oldComplexe) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE, oldComplexe, complexe));
			}
		}
		return complexe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Complexe basicGetComplexe() {
		return complexe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComplexe(Complexe newComplexe) {
		Complexe oldComplexe = complexe;
		complexe = newComplexe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE, oldComplexe, complexe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Requiert> getRequiert() {
		if (requiert == null) {
			requiert = new EObjectResolvingEList<Requiert>(Requiert.class, this, PaquetagePackage.APPLICATION_LOGICIELLE__REQUIERT);
		}
		return requiert;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PaquetagePackage.APPLICATION_LOGICIELLE__NOM:
				return getNom();
			case PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE:
				if (resolve) return getSimple();
				return basicGetSimple();
			case PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE:
				if (resolve) return getComplexe();
				return basicGetComplexe();
			case PaquetagePackage.APPLICATION_LOGICIELLE__REQUIERT:
				return getRequiert();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PaquetagePackage.APPLICATION_LOGICIELLE__NOM:
				setNom((String)newValue);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE:
				setSimple((Simple)newValue);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE:
				setComplexe((Complexe)newValue);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__REQUIERT:
				getRequiert().clear();
				getRequiert().addAll((Collection<? extends Requiert>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PaquetagePackage.APPLICATION_LOGICIELLE__NOM:
				setNom(NOM_EDEFAULT);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE:
				setSimple((Simple)null);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE:
				setComplexe((Complexe)null);
				return;
			case PaquetagePackage.APPLICATION_LOGICIELLE__REQUIERT:
				getRequiert().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PaquetagePackage.APPLICATION_LOGICIELLE__NOM:
				return NOM_EDEFAULT == null ? nom != null : !NOM_EDEFAULT.equals(nom);
			case PaquetagePackage.APPLICATION_LOGICIELLE__SIMPLE:
				return simple != null;
			case PaquetagePackage.APPLICATION_LOGICIELLE__COMPLEXE:
				return complexe != null;
			case PaquetagePackage.APPLICATION_LOGICIELLE__REQUIERT:
				return requiert != null && !requiert.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nom: ");
		result.append(nom);
		result.append(')');
		return result.toString();
	}

} //ApplicationLogicielleImpl
