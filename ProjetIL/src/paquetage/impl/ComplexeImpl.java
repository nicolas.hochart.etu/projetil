/**
 */
package paquetage.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import paquetage.Complexe;
import paquetage.PaquetagePackage;
import paquetage.PartieApplication;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complexe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link paquetage.impl.ComplexeImpl#getPartieapplication <em>Partieapplication</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexeImpl extends MinimalEObjectImpl.Container implements Complexe {
	/**
	 * The cached value of the '{@link #getPartieapplication() <em>Partieapplication</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartieapplication()
	 * @generated
	 * @ordered
	 */
	protected EList<PartieApplication> partieapplication;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PaquetagePackage.Literals.COMPLEXE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartieApplication> getPartieapplication() {
		if (partieapplication == null) {
			partieapplication = new EObjectResolvingEList<PartieApplication>(PartieApplication.class, this, PaquetagePackage.COMPLEXE__PARTIEAPPLICATION);
		}
		return partieapplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PaquetagePackage.COMPLEXE__PARTIEAPPLICATION:
				return getPartieapplication();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PaquetagePackage.COMPLEXE__PARTIEAPPLICATION:
				getPartieapplication().clear();
				getPartieapplication().addAll((Collection<? extends PartieApplication>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PaquetagePackage.COMPLEXE__PARTIEAPPLICATION:
				getPartieapplication().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PaquetagePackage.COMPLEXE__PARTIEAPPLICATION:
				return partieapplication != null && !partieapplication.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComplexeImpl
