/**
 */
package paquetage.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import paquetage.PaquetagePackage;
import paquetage.PartieApplication;
import paquetage.PortEntrée;
import paquetage.PortSortie;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Partie Application</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link paquetage.impl.PartieApplicationImpl#getNom <em>Nom</em>}</li>
 *   <li>{@link paquetage.impl.PartieApplicationImpl#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link paquetage.impl.PartieApplicationImpl#getPortentrée <em>Portentrée</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PartieApplicationImpl extends MinimalEObjectImpl.Container implements PartieApplication {
	/**
	 * The default value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected static final String NOM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected String nom = NOM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortsortie() <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortsortie()
	 * @generated
	 * @ordered
	 */
	protected PortSortie portsortie;

	/**
	 * The cached value of the '{@link #getPortentrée() <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortentrée()
	 * @generated
	 * @ordered
	 */
	protected PortEntrée portentrée;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PartieApplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PaquetagePackage.Literals.PARTIE_APPLICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNom(String newNom) {
		String oldNom = nom;
		nom = newNom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.PARTIE_APPLICATION__NOM, oldNom, nom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie getPortsortie() {
		if (portsortie != null && portsortie.eIsProxy()) {
			InternalEObject oldPortsortie = (InternalEObject)portsortie;
			portsortie = (PortSortie)eResolveProxy(oldPortsortie);
			if (portsortie != oldPortsortie) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE, oldPortsortie, portsortie));
			}
		}
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie basicGetPortsortie() {
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortsortie(PortSortie newPortsortie) {
		PortSortie oldPortsortie = portsortie;
		portsortie = newPortsortie;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE, oldPortsortie, portsortie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée getPortentrée() {
		if (portentrée != null && portentrée.eIsProxy()) {
			InternalEObject oldPortentrée = (InternalEObject)portentrée;
			portentrée = (PortEntrée)eResolveProxy(oldPortentrée);
			if (portentrée != oldPortentrée) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE, oldPortentrée, portentrée));
			}
		}
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée basicGetPortentrée() {
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortentrée(PortEntrée newPortentrée) {
		PortEntrée oldPortentrée = portentrée;
		portentrée = newPortentrée;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE, oldPortentrée, portentrée));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PaquetagePackage.PARTIE_APPLICATION__NOM:
				return getNom();
			case PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE:
				if (resolve) return getPortsortie();
				return basicGetPortsortie();
			case PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE:
				if (resolve) return getPortentrée();
				return basicGetPortentrée();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PaquetagePackage.PARTIE_APPLICATION__NOM:
				setNom((String)newValue);
				return;
			case PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE:
				setPortsortie((PortSortie)newValue);
				return;
			case PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE:
				setPortentrée((PortEntrée)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PaquetagePackage.PARTIE_APPLICATION__NOM:
				setNom(NOM_EDEFAULT);
				return;
			case PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE:
				setPortsortie((PortSortie)null);
				return;
			case PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE:
				setPortentrée((PortEntrée)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PaquetagePackage.PARTIE_APPLICATION__NOM:
				return NOM_EDEFAULT == null ? nom != null : !NOM_EDEFAULT.equals(nom);
			case PaquetagePackage.PARTIE_APPLICATION__PORTSORTIE:
				return portsortie != null;
			case PaquetagePackage.PARTIE_APPLICATION__PORTENTRÉE:
				return portentrée != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nom: ");
		result.append(nom);
		result.append(')');
		return result.toString();
	}

} //PartieApplicationImpl
