/**
 */
package paquetage.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import paquetage.Connecteur;
import paquetage.PaquetagePackage;
import paquetage.PortEntrée;
import paquetage.PortSortie;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link paquetage.impl.ConnecteurImpl#getNumero <em>Numero</em>}</li>
 *   <li>{@link paquetage.impl.ConnecteurImpl#getPortentrée <em>Portentrée</em>}</li>
 *   <li>{@link paquetage.impl.ConnecteurImpl#getPortsortie <em>Portsortie</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnecteurImpl extends MinimalEObjectImpl.Container implements Connecteur {
	/**
	 * The default value of the '{@link #getNumero() <em>Numero</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumero()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMERO_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumero() <em>Numero</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumero()
	 * @generated
	 * @ordered
	 */
	protected int numero = NUMERO_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortentrée() <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortentrée()
	 * @generated
	 * @ordered
	 */
	protected PortEntrée portentrée;

	/**
	 * The cached value of the '{@link #getPortsortie() <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortsortie()
	 * @generated
	 * @ordered
	 */
	protected PortSortie portsortie;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnecteurImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PaquetagePackage.Literals.CONNECTEUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumero(int newNumero) {
		int oldNumero = numero;
		numero = newNumero;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.CONNECTEUR__NUMERO, oldNumero, numero));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée getPortentrée() {
		if (portentrée != null && portentrée.eIsProxy()) {
			InternalEObject oldPortentrée = (InternalEObject)portentrée;
			portentrée = (PortEntrée)eResolveProxy(oldPortentrée);
			if (portentrée != oldPortentrée) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.CONNECTEUR__PORTENTRÉE, oldPortentrée, portentrée));
			}
		}
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée basicGetPortentrée() {
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortentrée(PortEntrée newPortentrée) {
		PortEntrée oldPortentrée = portentrée;
		portentrée = newPortentrée;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.CONNECTEUR__PORTENTRÉE, oldPortentrée, portentrée));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie getPortsortie() {
		if (portsortie != null && portsortie.eIsProxy()) {
			InternalEObject oldPortsortie = (InternalEObject)portsortie;
			portsortie = (PortSortie)eResolveProxy(oldPortsortie);
			if (portsortie != oldPortsortie) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PaquetagePackage.CONNECTEUR__PORTSORTIE, oldPortsortie, portsortie));
			}
		}
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie basicGetPortsortie() {
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortsortie(PortSortie newPortsortie) {
		PortSortie oldPortsortie = portsortie;
		portsortie = newPortsortie;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.CONNECTEUR__PORTSORTIE, oldPortsortie, portsortie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PaquetagePackage.CONNECTEUR__NUMERO:
				return getNumero();
			case PaquetagePackage.CONNECTEUR__PORTENTRÉE:
				if (resolve) return getPortentrée();
				return basicGetPortentrée();
			case PaquetagePackage.CONNECTEUR__PORTSORTIE:
				if (resolve) return getPortsortie();
				return basicGetPortsortie();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PaquetagePackage.CONNECTEUR__NUMERO:
				setNumero((Integer)newValue);
				return;
			case PaquetagePackage.CONNECTEUR__PORTENTRÉE:
				setPortentrée((PortEntrée)newValue);
				return;
			case PaquetagePackage.CONNECTEUR__PORTSORTIE:
				setPortsortie((PortSortie)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PaquetagePackage.CONNECTEUR__NUMERO:
				setNumero(NUMERO_EDEFAULT);
				return;
			case PaquetagePackage.CONNECTEUR__PORTENTRÉE:
				setPortentrée((PortEntrée)null);
				return;
			case PaquetagePackage.CONNECTEUR__PORTSORTIE:
				setPortsortie((PortSortie)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PaquetagePackage.CONNECTEUR__NUMERO:
				return numero != NUMERO_EDEFAULT;
			case PaquetagePackage.CONNECTEUR__PORTENTRÉE:
				return portentrée != null;
			case PaquetagePackage.CONNECTEUR__PORTSORTIE:
				return portsortie != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numero: ");
		result.append(numero);
		result.append(')');
		return result.toString();
	}

} //ConnecteurImpl
