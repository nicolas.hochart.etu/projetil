/**
 */
package paquetage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Partie Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.PartieApplication#getNom <em>Nom</em>}</li>
 *   <li>{@link paquetage.PartieApplication#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link paquetage.PartieApplication#getPortentrée <em>Portentrée</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getPartieApplication()
 * @model
 * @generated
 */
public interface PartieApplication extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see paquetage.PaquetagePackage#getPartieApplication_Nom()
	 * @model
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link paquetage.PartieApplication#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portsortie</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portsortie</em>' reference.
	 * @see #setPortsortie(PortSortie)
	 * @see paquetage.PaquetagePackage#getPartieApplication_Portsortie()
	 * @model
	 * @generated
	 */
	PortSortie getPortsortie();

	/**
	 * Sets the value of the '{@link paquetage.PartieApplication#getPortsortie <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portsortie</em>' reference.
	 * @see #getPortsortie()
	 * @generated
	 */
	void setPortsortie(PortSortie value);

	/**
	 * Returns the value of the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portentrée</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portentrée</em>' reference.
	 * @see #setPortentrée(PortEntrée)
	 * @see paquetage.PaquetagePackage#getPartieApplication_Portentrée()
	 * @model
	 * @generated
	 */
	PortEntrée getPortentrée();

	/**
	 * Sets the value of the '{@link paquetage.PartieApplication#getPortentrée <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portentrée</em>' reference.
	 * @see #getPortentrée()
	 * @generated
	 */
	void setPortentrée(PortEntrée value);

} // PartieApplication
