/**
 */
package paquetage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see paquetage.PaquetagePackage#getSimple()
 * @model
 * @generated
 */
public interface Simple extends EObject {
} // Simple
