/**
 */
package paquetage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see paquetage.PaquetagePackage
 * @generated
 */
public interface PaquetageFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PaquetageFactory eINSTANCE = paquetage.impl.PaquetageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Application Logicielle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Application Logicielle</em>'.
	 * @generated
	 */
	ApplicationLogicielle createApplicationLogicielle();

	/**
	 * Returns a new object of class '<em>Partie Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Partie Application</em>'.
	 * @generated
	 */
	PartieApplication createPartieApplication();

	/**
	 * Returns a new object of class '<em>Port Entrée</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Entrée</em>'.
	 * @generated
	 */
	PortEntrée createPortEntrée();

	/**
	 * Returns a new object of class '<em>Port Sortie</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Sortie</em>'.
	 * @generated
	 */
	PortSortie createPortSortie();

	/**
	 * Returns a new object of class '<em>Connecteur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connecteur</em>'.
	 * @generated
	 */
	Connecteur createConnecteur();

	/**
	 * Returns a new object of class '<em>Ressource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ressource</em>'.
	 * @generated
	 */
	Ressource createRessource();

	/**
	 * Returns a new object of class '<em>Simple</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple</em>'.
	 * @generated
	 */
	Simple createSimple();

	/**
	 * Returns a new object of class '<em>Complexe</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complexe</em>'.
	 * @generated
	 */
	Complexe createComplexe();

	/**
	 * Returns a new object of class '<em>Requiert</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requiert</em>'.
	 * @generated
	 */
	Requiert createRequiert();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PaquetagePackage getPaquetagePackage();

} //PaquetageFactory
