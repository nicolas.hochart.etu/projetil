/**
 */
package paquetage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see paquetage.PaquetageFactory
 * @model kind="package"
 * @generated
 */
public interface PaquetagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "paquetage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///paquetage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "paquetage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PaquetagePackage eINSTANCE = paquetage.impl.PaquetagePackageImpl.init();

	/**
	 * The meta object id for the '{@link paquetage.impl.ApplicationLogicielleImpl <em>Application Logicielle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.ApplicationLogicielleImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getApplicationLogicielle()
	 * @generated
	 */
	int APPLICATION_LOGICIELLE = 0;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__NOM = 0;

	/**
	 * The feature id for the '<em><b>Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__SIMPLE = 1;

	/**
	 * The feature id for the '<em><b>Complexe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__COMPLEXE = 2;

	/**
	 * The feature id for the '<em><b>Requiert</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__REQUIERT = 3;

	/**
	 * The number of structural features of the '<em>Application Logicielle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Application Logicielle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.PartieApplicationImpl <em>Partie Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.PartieApplicationImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getPartieApplication()
	 * @generated
	 */
	int PARTIE_APPLICATION = 1;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__NOM = 0;

	/**
	 * The feature id for the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__PORTSORTIE = 1;

	/**
	 * The feature id for the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__PORTENTRÉE = 2;

	/**
	 * The number of structural features of the '<em>Partie Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Partie Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.PortEntréeImpl <em>Port Entrée</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.PortEntréeImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getPortEntrée()
	 * @generated
	 */
	int PORT_ENTRÉE = 2;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE__NOM = 0;

	/**
	 * The number of structural features of the '<em>Port Entrée</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Port Entrée</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.PortSortieImpl <em>Port Sortie</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.PortSortieImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getPortSortie()
	 * @generated
	 */
	int PORT_SORTIE = 3;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE__NOM = 0;

	/**
	 * The number of structural features of the '<em>Port Sortie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Port Sortie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.ConnecteurImpl <em>Connecteur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.ConnecteurImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getConnecteur()
	 * @generated
	 */
	int CONNECTEUR = 4;

	/**
	 * The feature id for the '<em><b>Numero</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__NUMERO = 0;

	/**
	 * The feature id for the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__PORTENTRÉE = 1;

	/**
	 * The feature id for the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__PORTSORTIE = 2;

	/**
	 * The number of structural features of the '<em>Connecteur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Connecteur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.RessourceImpl <em>Ressource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.RessourceImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getRessource()
	 * @generated
	 */
	int RESSOURCE = 5;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE__NOM = 0;

	/**
	 * The number of structural features of the '<em>Ressource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Ressource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.SimpleImpl <em>Simple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.SimpleImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getSimple()
	 * @generated
	 */
	int SIMPLE = 6;

	/**
	 * The number of structural features of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.ComplexeImpl <em>Complexe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.ComplexeImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getComplexe()
	 * @generated
	 */
	int COMPLEXE = 7;

	/**
	 * The feature id for the '<em><b>Partieapplication</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__PARTIEAPPLICATION = 0;

	/**
	 * The number of structural features of the '<em>Complexe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Complexe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.RequiertImpl <em>Requiert</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.RequiertImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getRequiert()
	 * @generated
	 */
	int REQUIERT = 8;

	/**
	 * The feature id for the '<em><b>Ressource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIERT__RESSOURCE = 0;

	/**
	 * The number of structural features of the '<em>Requiert</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIERT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Requiert</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIERT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link paquetage.ApplicationLogicielle <em>Application Logicielle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application Logicielle</em>'.
	 * @see paquetage.ApplicationLogicielle
	 * @generated
	 */
	EClass getApplicationLogicielle();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.ApplicationLogicielle#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.ApplicationLogicielle#getNom()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EAttribute getApplicationLogicielle_Nom();

	/**
	 * Returns the meta object for the reference '{@link paquetage.ApplicationLogicielle#getSimple <em>Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Simple</em>'.
	 * @see paquetage.ApplicationLogicielle#getSimple()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EReference getApplicationLogicielle_Simple();

	/**
	 * Returns the meta object for the reference '{@link paquetage.ApplicationLogicielle#getComplexe <em>Complexe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Complexe</em>'.
	 * @see paquetage.ApplicationLogicielle#getComplexe()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EReference getApplicationLogicielle_Complexe();

	/**
	 * Returns the meta object for the reference list '{@link paquetage.ApplicationLogicielle#getRequiert <em>Requiert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Requiert</em>'.
	 * @see paquetage.ApplicationLogicielle#getRequiert()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EReference getApplicationLogicielle_Requiert();

	/**
	 * Returns the meta object for class '{@link paquetage.PartieApplication <em>Partie Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Partie Application</em>'.
	 * @see paquetage.PartieApplication
	 * @generated
	 */
	EClass getPartieApplication();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.PartieApplication#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.PartieApplication#getNom()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EAttribute getPartieApplication_Nom();

	/**
	 * Returns the meta object for the reference '{@link paquetage.PartieApplication#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portsortie</em>'.
	 * @see paquetage.PartieApplication#getPortsortie()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EReference getPartieApplication_Portsortie();

	/**
	 * Returns the meta object for the reference '{@link paquetage.PartieApplication#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portentrée</em>'.
	 * @see paquetage.PartieApplication#getPortentrée()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EReference getPartieApplication_Portentrée();

	/**
	 * Returns the meta object for class '{@link paquetage.PortEntrée <em>Port Entrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Entrée</em>'.
	 * @see paquetage.PortEntrée
	 * @generated
	 */
	EClass getPortEntrée();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.PortEntrée#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.PortEntrée#getNom()
	 * @see #getPortEntrée()
	 * @generated
	 */
	EAttribute getPortEntrée_Nom();

	/**
	 * Returns the meta object for class '{@link paquetage.PortSortie <em>Port Sortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Sortie</em>'.
	 * @see paquetage.PortSortie
	 * @generated
	 */
	EClass getPortSortie();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.PortSortie#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.PortSortie#getNom()
	 * @see #getPortSortie()
	 * @generated
	 */
	EAttribute getPortSortie_Nom();

	/**
	 * Returns the meta object for class '{@link paquetage.Connecteur <em>Connecteur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur</em>'.
	 * @see paquetage.Connecteur
	 * @generated
	 */
	EClass getConnecteur();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.Connecteur#getNumero <em>Numero</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Numero</em>'.
	 * @see paquetage.Connecteur#getNumero()
	 * @see #getConnecteur()
	 * @generated
	 */
	EAttribute getConnecteur_Numero();

	/**
	 * Returns the meta object for the reference '{@link paquetage.Connecteur#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portentrée</em>'.
	 * @see paquetage.Connecteur#getPortentrée()
	 * @see #getConnecteur()
	 * @generated
	 */
	EReference getConnecteur_Portentrée();

	/**
	 * Returns the meta object for the reference '{@link paquetage.Connecteur#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portsortie</em>'.
	 * @see paquetage.Connecteur#getPortsortie()
	 * @see #getConnecteur()
	 * @generated
	 */
	EReference getConnecteur_Portsortie();

	/**
	 * Returns the meta object for class '{@link paquetage.Ressource <em>Ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ressource</em>'.
	 * @see paquetage.Ressource
	 * @generated
	 */
	EClass getRessource();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.Ressource#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.Ressource#getNom()
	 * @see #getRessource()
	 * @generated
	 */
	EAttribute getRessource_Nom();

	/**
	 * Returns the meta object for class '{@link paquetage.Simple <em>Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple</em>'.
	 * @see paquetage.Simple
	 * @generated
	 */
	EClass getSimple();

	/**
	 * Returns the meta object for class '{@link paquetage.Complexe <em>Complexe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complexe</em>'.
	 * @see paquetage.Complexe
	 * @generated
	 */
	EClass getComplexe();

	/**
	 * Returns the meta object for the reference list '{@link paquetage.Complexe#getPartieapplication <em>Partieapplication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Partieapplication</em>'.
	 * @see paquetage.Complexe#getPartieapplication()
	 * @see #getComplexe()
	 * @generated
	 */
	EReference getComplexe_Partieapplication();

	/**
	 * Returns the meta object for class '{@link paquetage.Requiert <em>Requiert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requiert</em>'.
	 * @see paquetage.Requiert
	 * @generated
	 */
	EClass getRequiert();

	/**
	 * Returns the meta object for the reference '{@link paquetage.Requiert#getRessource <em>Ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ressource</em>'.
	 * @see paquetage.Requiert#getRessource()
	 * @see #getRequiert()
	 * @generated
	 */
	EReference getRequiert_Ressource();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PaquetageFactory getPaquetageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link paquetage.impl.ApplicationLogicielleImpl <em>Application Logicielle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.ApplicationLogicielleImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getApplicationLogicielle()
		 * @generated
		 */
		EClass APPLICATION_LOGICIELLE = eINSTANCE.getApplicationLogicielle();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION_LOGICIELLE__NOM = eINSTANCE.getApplicationLogicielle_Nom();

		/**
		 * The meta object literal for the '<em><b>Simple</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_LOGICIELLE__SIMPLE = eINSTANCE.getApplicationLogicielle_Simple();

		/**
		 * The meta object literal for the '<em><b>Complexe</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_LOGICIELLE__COMPLEXE = eINSTANCE.getApplicationLogicielle_Complexe();

		/**
		 * The meta object literal for the '<em><b>Requiert</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_LOGICIELLE__REQUIERT = eINSTANCE.getApplicationLogicielle_Requiert();

		/**
		 * The meta object literal for the '{@link paquetage.impl.PartieApplicationImpl <em>Partie Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.PartieApplicationImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getPartieApplication()
		 * @generated
		 */
		EClass PARTIE_APPLICATION = eINSTANCE.getPartieApplication();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARTIE_APPLICATION__NOM = eINSTANCE.getPartieApplication_Nom();

		/**
		 * The meta object literal for the '<em><b>Portsortie</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTIE_APPLICATION__PORTSORTIE = eINSTANCE.getPartieApplication_Portsortie();

		/**
		 * The meta object literal for the '<em><b>Portentrée</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTIE_APPLICATION__PORTENTRÉE = eINSTANCE.getPartieApplication_Portentrée();

		/**
		 * The meta object literal for the '{@link paquetage.impl.PortEntréeImpl <em>Port Entrée</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.PortEntréeImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getPortEntrée()
		 * @generated
		 */
		EClass PORT_ENTRÉE = eINSTANCE.getPortEntrée();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_ENTRÉE__NOM = eINSTANCE.getPortEntrée_Nom();

		/**
		 * The meta object literal for the '{@link paquetage.impl.PortSortieImpl <em>Port Sortie</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.PortSortieImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getPortSortie()
		 * @generated
		 */
		EClass PORT_SORTIE = eINSTANCE.getPortSortie();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_SORTIE__NOM = eINSTANCE.getPortSortie_Nom();

		/**
		 * The meta object literal for the '{@link paquetage.impl.ConnecteurImpl <em>Connecteur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.ConnecteurImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getConnecteur()
		 * @generated
		 */
		EClass CONNECTEUR = eINSTANCE.getConnecteur();

		/**
		 * The meta object literal for the '<em><b>Numero</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTEUR__NUMERO = eINSTANCE.getConnecteur_Numero();

		/**
		 * The meta object literal for the '<em><b>Portentrée</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR__PORTENTRÉE = eINSTANCE.getConnecteur_Portentrée();

		/**
		 * The meta object literal for the '<em><b>Portsortie</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR__PORTSORTIE = eINSTANCE.getConnecteur_Portsortie();

		/**
		 * The meta object literal for the '{@link paquetage.impl.RessourceImpl <em>Ressource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.RessourceImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getRessource()
		 * @generated
		 */
		EClass RESSOURCE = eINSTANCE.getRessource();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESSOURCE__NOM = eINSTANCE.getRessource_Nom();

		/**
		 * The meta object literal for the '{@link paquetage.impl.SimpleImpl <em>Simple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.SimpleImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getSimple()
		 * @generated
		 */
		EClass SIMPLE = eINSTANCE.getSimple();

		/**
		 * The meta object literal for the '{@link paquetage.impl.ComplexeImpl <em>Complexe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.ComplexeImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getComplexe()
		 * @generated
		 */
		EClass COMPLEXE = eINSTANCE.getComplexe();

		/**
		 * The meta object literal for the '<em><b>Partieapplication</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEXE__PARTIEAPPLICATION = eINSTANCE.getComplexe_Partieapplication();

		/**
		 * The meta object literal for the '{@link paquetage.impl.RequiertImpl <em>Requiert</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.RequiertImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getRequiert()
		 * @generated
		 */
		EClass REQUIERT = eINSTANCE.getRequiert();

		/**
		 * The meta object literal for the '<em><b>Ressource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIERT__RESSOURCE = eINSTANCE.getRequiert_Ressource();

	}

} //PaquetagePackage
