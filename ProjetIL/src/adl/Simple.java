/**
 */
package adl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.Simple#getSimple_ressource <em>Simple ressource</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getSimple()
 * @model
 * @generated
 */
public interface Simple extends ApplicationLogicielle {

	/**
	 * Returns the value of the '<em><b>Simple ressource</b></em>' reference list.
	 * The list contents are of type {@link adl.Ressource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple ressource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple ressource</em>' reference list.
	 * @see adl.AdlPackage#getSimple_Simple_ressource()
	 * @model
	 * @generated
	 */
	EList<Ressource> getSimple_ressource();
} // Simple
