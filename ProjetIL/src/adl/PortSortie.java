/**
 */
package adl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Sortie</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.PortSortie#getPortentrée <em>Portentrée</em>}</li>
 *   <li>{@link adl.PortSortie#getConnecteursortie <em>Connecteursortie</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getPortSortie()
 * @model
 * @generated
 */
public interface PortSortie extends Port {
	/**
	 * Returns the value of the '<em><b>Portentrée</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.PortEntrée#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portentrée</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portentrée</em>' reference.
	 * @see #setPortentrée(PortEntrée)
	 * @see adl.AdlPackage#getPortSortie_Portentrée()
	 * @see adl.PortEntrée#getPortsortie
	 * @model opposite="portsortie" required="true"
	 * @generated
	 */
	PortEntrée getPortentrée();

	/**
	 * Sets the value of the '{@link adl.PortSortie#getPortentrée <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portentrée</em>' reference.
	 * @see #getPortentrée()
	 * @generated
	 */
	void setPortentrée(PortEntrée value);

	/**
	 * Returns the value of the '<em><b>Connecteursortie</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.Connecteur#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteursortie</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteursortie</em>' reference.
	 * @see #setConnecteursortie(Connecteur)
	 * @see adl.AdlPackage#getPortSortie_Connecteursortie()
	 * @see adl.Connecteur#getPortsortie
	 * @model opposite="portsortie" required="true"
	 * @generated
	 */
	Connecteur getConnecteursortie();

	/**
	 * Sets the value of the '{@link adl.PortSortie#getConnecteursortie <em>Connecteursortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connecteursortie</em>' reference.
	 * @see #getConnecteursortie()
	 * @generated
	 */
	void setConnecteursortie(Connecteur value);

} // PortSortie
