/**
 */
package adl.impl;

import adl.AdlPackage;
import adl.Connecteur;
import adl.PortEntrée;
import adl.PortSortie;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Sortie</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link adl.impl.PortSortieImpl#getPortentrée <em>Portentrée</em>}</li>
 *   <li>{@link adl.impl.PortSortieImpl#getConnecteursortie <em>Connecteursortie</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortSortieImpl extends PortImpl implements PortSortie {
	/**
	 * The cached value of the '{@link #getPortentrée() <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortentrée()
	 * @generated
	 * @ordered
	 */
	protected PortEntrée portentrée;

	/**
	 * The cached value of the '{@link #getConnecteursortie() <em>Connecteursortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteursortie()
	 * @generated
	 * @ordered
	 */
	protected Connecteur connecteursortie;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortSortieImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdlPackage.Literals.PORT_SORTIE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée getPortentrée() {
		if (portentrée != null && portentrée.eIsProxy()) {
			InternalEObject oldPortentrée = (InternalEObject)portentrée;
			portentrée = (PortEntrée)eResolveProxy(oldPortentrée);
			if (portentrée != oldPortentrée) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.PORT_SORTIE__PORTENTRÉE, oldPortentrée, portentrée));
			}
		}
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée basicGetPortentrée() {
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortentrée(PortEntrée newPortentrée, NotificationChain msgs) {
		PortEntrée oldPortentrée = portentrée;
		portentrée = newPortentrée;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_SORTIE__PORTENTRÉE, oldPortentrée, newPortentrée);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortentrée(PortEntrée newPortentrée) {
		if (newPortentrée != portentrée) {
			NotificationChain msgs = null;
			if (portentrée != null)
				msgs = ((InternalEObject)portentrée).eInverseRemove(this, AdlPackage.PORT_ENTRÉE__PORTSORTIE, PortEntrée.class, msgs);
			if (newPortentrée != null)
				msgs = ((InternalEObject)newPortentrée).eInverseAdd(this, AdlPackage.PORT_ENTRÉE__PORTSORTIE, PortEntrée.class, msgs);
			msgs = basicSetPortentrée(newPortentrée, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_SORTIE__PORTENTRÉE, newPortentrée, newPortentrée));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur getConnecteursortie() {
		if (connecteursortie != null && connecteursortie.eIsProxy()) {
			InternalEObject oldConnecteursortie = (InternalEObject)connecteursortie;
			connecteursortie = (Connecteur)eResolveProxy(oldConnecteursortie);
			if (connecteursortie != oldConnecteursortie) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, oldConnecteursortie, connecteursortie));
			}
		}
		return connecteursortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur basicGetConnecteursortie() {
		return connecteursortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConnecteursortie(Connecteur newConnecteursortie, NotificationChain msgs) {
		Connecteur oldConnecteursortie = connecteursortie;
		connecteursortie = newConnecteursortie;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, oldConnecteursortie, newConnecteursortie);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnecteursortie(Connecteur newConnecteursortie) {
		if (newConnecteursortie != connecteursortie) {
			NotificationChain msgs = null;
			if (connecteursortie != null)
				msgs = ((InternalEObject)connecteursortie).eInverseRemove(this, AdlPackage.CONNECTEUR__PORTSORTIE, Connecteur.class, msgs);
			if (newConnecteursortie != null)
				msgs = ((InternalEObject)newConnecteursortie).eInverseAdd(this, AdlPackage.CONNECTEUR__PORTSORTIE, Connecteur.class, msgs);
			msgs = basicSetConnecteursortie(newConnecteursortie, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, newConnecteursortie, newConnecteursortie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				if (portentrée != null)
					msgs = ((InternalEObject)portentrée).eInverseRemove(this, AdlPackage.PORT_ENTRÉE__PORTSORTIE, PortEntrée.class, msgs);
				return basicSetPortentrée((PortEntrée)otherEnd, msgs);
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				if (connecteursortie != null)
					msgs = ((InternalEObject)connecteursortie).eInverseRemove(this, AdlPackage.CONNECTEUR__PORTSORTIE, Connecteur.class, msgs);
				return basicSetConnecteursortie((Connecteur)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				return basicSetPortentrée(null, msgs);
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				return basicSetConnecteursortie(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				if (resolve) return getPortentrée();
				return basicGetPortentrée();
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				if (resolve) return getConnecteursortie();
				return basicGetConnecteursortie();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				setPortentrée((PortEntrée)newValue);
				return;
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				setConnecteursortie((Connecteur)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				setPortentrée((PortEntrée)null);
				return;
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				setConnecteursortie((Connecteur)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdlPackage.PORT_SORTIE__PORTENTRÉE:
				return portentrée != null;
			case AdlPackage.PORT_SORTIE__CONNECTEURSORTIE:
				return connecteursortie != null;
		}
		return super.eIsSet(featureID);
	}

} //PortSortieImpl
