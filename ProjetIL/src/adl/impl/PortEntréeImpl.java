/**
 */
package adl.impl;

import adl.AdlPackage;
import adl.Connecteur;
import adl.PortEntrée;

import adl.PortSortie;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Entrée</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link adl.impl.PortEntréeImpl#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link adl.impl.PortEntréeImpl#getConnecteurentrée <em>Connecteurentrée</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortEntréeImpl extends PortImpl implements PortEntrée {
	/**
	 * The cached value of the '{@link #getPortsortie() <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortsortie()
	 * @generated
	 * @ordered
	 */
	protected PortSortie portsortie;

	/**
	 * The cached value of the '{@link #getConnecteurentrée() <em>Connecteurentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteurentrée()
	 * @generated
	 * @ordered
	 */
	protected Connecteur connecteurentrée;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortEntréeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdlPackage.Literals.PORT_ENTRÉE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie getPortsortie() {
		if (portsortie != null && portsortie.eIsProxy()) {
			InternalEObject oldPortsortie = (InternalEObject)portsortie;
			portsortie = (PortSortie)eResolveProxy(oldPortsortie);
			if (portsortie != oldPortsortie) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.PORT_ENTRÉE__PORTSORTIE, oldPortsortie, portsortie));
			}
		}
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie basicGetPortsortie() {
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortsortie(PortSortie newPortsortie, NotificationChain msgs) {
		PortSortie oldPortsortie = portsortie;
		portsortie = newPortsortie;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_ENTRÉE__PORTSORTIE, oldPortsortie, newPortsortie);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortsortie(PortSortie newPortsortie) {
		if (newPortsortie != portsortie) {
			NotificationChain msgs = null;
			if (portsortie != null)
				msgs = ((InternalEObject)portsortie).eInverseRemove(this, AdlPackage.PORT_SORTIE__PORTENTRÉE, PortSortie.class, msgs);
			if (newPortsortie != null)
				msgs = ((InternalEObject)newPortsortie).eInverseAdd(this, AdlPackage.PORT_SORTIE__PORTENTRÉE, PortSortie.class, msgs);
			msgs = basicSetPortsortie(newPortsortie, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_ENTRÉE__PORTSORTIE, newPortsortie, newPortsortie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur getConnecteurentrée() {
		if (connecteurentrée != null && connecteurentrée.eIsProxy()) {
			InternalEObject oldConnecteurentrée = (InternalEObject)connecteurentrée;
			connecteurentrée = (Connecteur)eResolveProxy(oldConnecteurentrée);
			if (connecteurentrée != oldConnecteurentrée) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, oldConnecteurentrée, connecteurentrée));
			}
		}
		return connecteurentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur basicGetConnecteurentrée() {
		return connecteurentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConnecteurentrée(Connecteur newConnecteurentrée, NotificationChain msgs) {
		Connecteur oldConnecteurentrée = connecteurentrée;
		connecteurentrée = newConnecteurentrée;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, oldConnecteurentrée, newConnecteurentrée);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnecteurentrée(Connecteur newConnecteurentrée) {
		if (newConnecteurentrée != connecteurentrée) {
			NotificationChain msgs = null;
			if (connecteurentrée != null)
				msgs = ((InternalEObject)connecteurentrée).eInverseRemove(this, AdlPackage.CONNECTEUR__PORTENTRÉE, Connecteur.class, msgs);
			if (newConnecteurentrée != null)
				msgs = ((InternalEObject)newConnecteurentrée).eInverseAdd(this, AdlPackage.CONNECTEUR__PORTENTRÉE, Connecteur.class, msgs);
			msgs = basicSetConnecteurentrée(newConnecteurentrée, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, newConnecteurentrée, newConnecteurentrée));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				if (portsortie != null)
					msgs = ((InternalEObject)portsortie).eInverseRemove(this, AdlPackage.PORT_SORTIE__PORTENTRÉE, PortSortie.class, msgs);
				return basicSetPortsortie((PortSortie)otherEnd, msgs);
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				if (connecteurentrée != null)
					msgs = ((InternalEObject)connecteurentrée).eInverseRemove(this, AdlPackage.CONNECTEUR__PORTENTRÉE, Connecteur.class, msgs);
				return basicSetConnecteurentrée((Connecteur)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				return basicSetPortsortie(null, msgs);
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				return basicSetConnecteurentrée(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				if (resolve) return getPortsortie();
				return basicGetPortsortie();
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				if (resolve) return getConnecteurentrée();
				return basicGetConnecteurentrée();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				setPortsortie((PortSortie)newValue);
				return;
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				setConnecteurentrée((Connecteur)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				setPortsortie((PortSortie)null);
				return;
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				setConnecteurentrée((Connecteur)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdlPackage.PORT_ENTRÉE__PORTSORTIE:
				return portsortie != null;
			case AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE:
				return connecteurentrée != null;
		}
		return super.eIsSet(featureID);
	}

} //PortEntréeImpl
