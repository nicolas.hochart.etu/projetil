/**
 */
package adl.impl;

import adl.AdlPackage;
import adl.Ressource;
import adl.Simple;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link adl.impl.SimpleImpl#getSimple_ressource <em>Simple ressource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimpleImpl extends ApplicationLogicielleImpl implements Simple {
	/**
	 * The cached value of the '{@link #getSimple_ressource() <em>Simple ressource</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimple_ressource()
	 * @generated
	 * @ordered
	 */
	protected EList<Ressource> simple_ressource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdlPackage.Literals.SIMPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Ressource> getSimple_ressource() {
		if (simple_ressource == null) {
			simple_ressource = new EObjectResolvingEList<Ressource>(Ressource.class, this, AdlPackage.SIMPLE__SIMPLE_RESSOURCE);
		}
		return simple_ressource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdlPackage.SIMPLE__SIMPLE_RESSOURCE:
				return getSimple_ressource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdlPackage.SIMPLE__SIMPLE_RESSOURCE:
				getSimple_ressource().clear();
				getSimple_ressource().addAll((Collection<? extends Ressource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdlPackage.SIMPLE__SIMPLE_RESSOURCE:
				getSimple_ressource().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdlPackage.SIMPLE__SIMPLE_RESSOURCE:
				return simple_ressource != null && !simple_ressource.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SimpleImpl
