/**
 */
package adl.impl;

import adl.AdlPackage;
import adl.Complexe;
import adl.Connecteur;
import adl.PartieApplication;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complexe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link adl.impl.ComplexeImpl#getPartieapplication <em>Partieapplication</em>}</li>
 *   <li>{@link adl.impl.ComplexeImpl#getComplexe <em>Complexe</em>}</li>
 *   <li>{@link adl.impl.ComplexeImpl#getConnecteur <em>Connecteur</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexeImpl extends ApplicationLogicielleImpl implements Complexe {
	/**
	 * The cached value of the '{@link #getPartieapplication() <em>Partieapplication</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartieapplication()
	 * @generated
	 * @ordered
	 */
	protected EList<PartieApplication> partieapplication;

	/**
	 * The cached value of the '{@link #getComplexe() <em>Complexe</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComplexe()
	 * @generated
	 * @ordered
	 */
	protected EList<Complexe> complexe;

	/**
	 * The cached value of the '{@link #getConnecteur() <em>Connecteur</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteur()
	 * @generated
	 * @ordered
	 */
	protected EList<Connecteur> connecteur;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdlPackage.Literals.COMPLEXE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartieApplication> getPartieapplication() {
		if (partieapplication == null) {
			partieapplication = new EObjectContainmentEList<PartieApplication>(PartieApplication.class, this, AdlPackage.COMPLEXE__PARTIEAPPLICATION);
		}
		return partieapplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Complexe> getComplexe() {
		if (complexe == null) {
			complexe = new EObjectContainmentEList<Complexe>(Complexe.class, this, AdlPackage.COMPLEXE__COMPLEXE);
		}
		return complexe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connecteur> getConnecteur() {
		if (connecteur == null) {
			connecteur = new EObjectContainmentEList<Connecteur>(Connecteur.class, this, AdlPackage.COMPLEXE__CONNECTEUR);
		}
		return connecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.COMPLEXE__PARTIEAPPLICATION:
				return ((InternalEList<?>)getPartieapplication()).basicRemove(otherEnd, msgs);
			case AdlPackage.COMPLEXE__COMPLEXE:
				return ((InternalEList<?>)getComplexe()).basicRemove(otherEnd, msgs);
			case AdlPackage.COMPLEXE__CONNECTEUR:
				return ((InternalEList<?>)getConnecteur()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdlPackage.COMPLEXE__PARTIEAPPLICATION:
				return getPartieapplication();
			case AdlPackage.COMPLEXE__COMPLEXE:
				return getComplexe();
			case AdlPackage.COMPLEXE__CONNECTEUR:
				return getConnecteur();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdlPackage.COMPLEXE__PARTIEAPPLICATION:
				getPartieapplication().clear();
				getPartieapplication().addAll((Collection<? extends PartieApplication>)newValue);
				return;
			case AdlPackage.COMPLEXE__COMPLEXE:
				getComplexe().clear();
				getComplexe().addAll((Collection<? extends Complexe>)newValue);
				return;
			case AdlPackage.COMPLEXE__CONNECTEUR:
				getConnecteur().clear();
				getConnecteur().addAll((Collection<? extends Connecteur>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdlPackage.COMPLEXE__PARTIEAPPLICATION:
				getPartieapplication().clear();
				return;
			case AdlPackage.COMPLEXE__COMPLEXE:
				getComplexe().clear();
				return;
			case AdlPackage.COMPLEXE__CONNECTEUR:
				getConnecteur().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdlPackage.COMPLEXE__PARTIEAPPLICATION:
				return partieapplication != null && !partieapplication.isEmpty();
			case AdlPackage.COMPLEXE__COMPLEXE:
				return complexe != null && !complexe.isEmpty();
			case AdlPackage.COMPLEXE__CONNECTEUR:
				return connecteur != null && !connecteur.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComplexeImpl
