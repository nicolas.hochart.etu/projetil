/**
 */
package adl.impl;

import adl.AdlFactory;
import adl.AdlPackage;
import adl.ApplicationLogicielle;
import adl.Complexe;
import adl.Connecteur;
import adl.PartieApplication;
import adl.Port;
import adl.PortEntrée;
import adl.PortSortie;
import adl.Ressource;
import adl.Simple;

import adl.util.AdlValidator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdlPackageImpl extends EPackageImpl implements AdlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicationLogicielleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass partieApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEntréeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portSortieEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ressourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connecteurEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see adl.AdlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AdlPackageImpl() {
		super(eNS_URI, AdlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AdlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AdlPackage init() {
		if (isInited) return (AdlPackage)EPackage.Registry.INSTANCE.getEPackage(AdlPackage.eNS_URI);

		// Obtain or create and register package
		AdlPackageImpl theAdlPackage = (AdlPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AdlPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AdlPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theAdlPackage.createPackageContents();

		// Initialize created meta-data
		theAdlPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theAdlPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return AdlValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theAdlPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AdlPackage.eNS_URI, theAdlPackage);
		return theAdlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicationLogicielle() {
		return applicationLogicielleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApplicationLogicielle_Nom() {
		return (EAttribute)applicationLogicielleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationLogicielle_Ressource() {
		return (EReference)applicationLogicielleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPartieApplication() {
		return partieApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPartieApplication_Nom() {
		return (EAttribute)partieApplicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPartieApplication_Port() {
		return (EReference)partieApplicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPartieApplication_Ressource() {
		return (EReference)partieApplicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortEntrée() {
		return portEntréeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortEntrée_Portsortie() {
		return (EReference)portEntréeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortEntrée_Connecteurentrée() {
		return (EReference)portEntréeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortSortie() {
		return portSortieEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortSortie_Portentrée() {
		return (EReference)portSortieEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortSortie_Connecteursortie() {
		return (EReference)portSortieEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRessource() {
		return ressourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRessource_Nom() {
		return (EAttribute)ressourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimple() {
		return simpleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimple_Simple_ressource() {
		return (EReference)simpleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexe() {
		return complexeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexe_Partieapplication() {
		return (EReference)complexeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexe_Complexe() {
		return (EReference)complexeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexe_Connecteur() {
		return (EReference)complexeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort() {
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Owner() {
		return (EReference)portEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Nom() {
		return (EAttribute)portEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnecteur() {
		return connecteurEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConnecteur_Num() {
		return (EAttribute)connecteurEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteur_Portsortie() {
		return (EReference)connecteurEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteur_Portentrée() {
		return (EReference)connecteurEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdlFactory getAdlFactory() {
		return (AdlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		applicationLogicielleEClass = createEClass(APPLICATION_LOGICIELLE);
		createEAttribute(applicationLogicielleEClass, APPLICATION_LOGICIELLE__NOM);
		createEReference(applicationLogicielleEClass, APPLICATION_LOGICIELLE__RESSOURCE);

		partieApplicationEClass = createEClass(PARTIE_APPLICATION);
		createEAttribute(partieApplicationEClass, PARTIE_APPLICATION__NOM);
		createEReference(partieApplicationEClass, PARTIE_APPLICATION__PORT);
		createEReference(partieApplicationEClass, PARTIE_APPLICATION__RESSOURCE);

		portEntréeEClass = createEClass(PORT_ENTRÉE);
		createEReference(portEntréeEClass, PORT_ENTRÉE__PORTSORTIE);
		createEReference(portEntréeEClass, PORT_ENTRÉE__CONNECTEURENTRÉE);

		portSortieEClass = createEClass(PORT_SORTIE);
		createEReference(portSortieEClass, PORT_SORTIE__PORTENTRÉE);
		createEReference(portSortieEClass, PORT_SORTIE__CONNECTEURSORTIE);

		ressourceEClass = createEClass(RESSOURCE);
		createEAttribute(ressourceEClass, RESSOURCE__NOM);

		simpleEClass = createEClass(SIMPLE);
		createEReference(simpleEClass, SIMPLE__SIMPLE_RESSOURCE);

		complexeEClass = createEClass(COMPLEXE);
		createEReference(complexeEClass, COMPLEXE__PARTIEAPPLICATION);
		createEReference(complexeEClass, COMPLEXE__COMPLEXE);
		createEReference(complexeEClass, COMPLEXE__CONNECTEUR);

		portEClass = createEClass(PORT);
		createEReference(portEClass, PORT__OWNER);
		createEAttribute(portEClass, PORT__NOM);

		connecteurEClass = createEClass(CONNECTEUR);
		createEAttribute(connecteurEClass, CONNECTEUR__NUM);
		createEReference(connecteurEClass, CONNECTEUR__PORTSORTIE);
		createEReference(connecteurEClass, CONNECTEUR__PORTENTRÉE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		portEntréeEClass.getESuperTypes().add(this.getPort());
		portSortieEClass.getESuperTypes().add(this.getPort());
		simpleEClass.getESuperTypes().add(this.getApplicationLogicielle());
		complexeEClass.getESuperTypes().add(this.getApplicationLogicielle());

		// Initialize classes, features, and operations; add parameters
		initEClass(applicationLogicielleEClass, ApplicationLogicielle.class, "ApplicationLogicielle", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getApplicationLogicielle_Nom(), ecorePackage.getEString(), "nom", null, 1, 1, ApplicationLogicielle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationLogicielle_Ressource(), this.getRessource(), null, "ressource", null, 0, -1, ApplicationLogicielle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(partieApplicationEClass, PartieApplication.class, "PartieApplication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPartieApplication_Nom(), ecorePackage.getEString(), "nom", null, 1, 1, PartieApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPartieApplication_Port(), this.getPort(), this.getPort_Owner(), "port", null, 0, -1, PartieApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPartieApplication_Ressource(), this.getRessource(), null, "ressource", null, 0, -1, PartieApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEntréeEClass, PortEntrée.class, "PortEntrée", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortEntrée_Portsortie(), this.getPortSortie(), this.getPortSortie_Portentrée(), "portsortie", null, 1, 1, PortEntrée.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortEntrée_Connecteurentrée(), this.getConnecteur(), this.getConnecteur_Portentrée(), "connecteurentrée", null, 1, 1, PortEntrée.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portSortieEClass, PortSortie.class, "PortSortie", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortSortie_Portentrée(), this.getPortEntrée(), this.getPortEntrée_Portsortie(), "portentrée", null, 1, 1, PortSortie.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortSortie_Connecteursortie(), this.getConnecteur(), this.getConnecteur_Portsortie(), "connecteursortie", null, 1, 1, PortSortie.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ressourceEClass, Ressource.class, "Ressource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRessource_Nom(), ecorePackage.getEString(), "nom", null, 1, 1, Ressource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleEClass, Simple.class, "Simple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimple_Simple_ressource(), this.getRessource(), null, "simple_ressource", null, 0, -1, Simple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexeEClass, Complexe.class, "Complexe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComplexe_Partieapplication(), this.getPartieApplication(), null, "partieapplication", null, 0, -1, Complexe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexe_Complexe(), this.getComplexe(), null, "complexe", null, 0, -1, Complexe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexe_Connecteur(), this.getConnecteur(), null, "connecteur", null, 0, -1, Complexe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Owner(), this.getPartieApplication(), this.getPartieApplication_Port(), "owner", null, 1, 1, Port.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Nom(), ecorePackage.getEString(), "nom", null, 1, 1, Port.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteurEClass, Connecteur.class, "Connecteur", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConnecteur_Num(), ecorePackage.getEInt(), "num", null, 1, 1, Connecteur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnecteur_Portsortie(), this.getPortSortie(), this.getPortSortie_Connecteursortie(), "portsortie", null, 1, 1, Connecteur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnecteur_Portentrée(), this.getPortEntrée(), this.getPortEntrée_Connecteurentrée(), "portentrée", null, 1, 1, Connecteur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
		// http://www.eclipse.org/OCL/Import
		createImportAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createImportAnnotations() {
		String source = "http://www.eclipse.org/OCL/Import";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "ecore", "http://www.eclipse.org/emf/2002/Ecore"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (applicationLogicielleEClass, 
		   source, 
		   new String[] {
			 "constraints", "uniciteNoms motsReserves"
		   });	
		addAnnotation
		  (partieApplicationEClass, 
		   source, 
		   new String[] {
			 "constraints", "uniciteNoms motsReserves"
		   });	
		addAnnotation
		  (ressourceEClass, 
		   source, 
		   new String[] {
			 "constraints", "motsReserves"
		   });	
		addAnnotation
		  (complexeEClass, 
		   source, 
		   new String[] {
			 "constraints", "uniciteNums"
		   });	
		addAnnotation
		  (portEClass, 
		   source, 
		   new String[] {
			 "constraints", "motsReserves"
		   });	
		addAnnotation
		  (connecteurEClass, 
		   source, 
		   new String[] {
			 "constraints", "numPositif"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (applicationLogicielleEClass, 
		   source, 
		   new String[] {
			 "uniciteNoms", "\n\t\t\tself.ressource -> forAll(r1,r2 : Ressource | r1.nom = r2.nom implies r1 = r2) ",
			 "motsReserves", "\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' and self.nom <> \'Port\'"
		   });	
		addAnnotation
		  (partieApplicationEClass, 
		   source, 
		   new String[] {
			 "uniciteNoms", "\n\t\t\tself.port -> forAll(p1,p2 : Port | p1.nom = p2.nom implies p1 = p2) ",
			 "motsReserves", "\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' and self.nom <> \'Port\'"
		   });	
		addAnnotation
		  (ressourceEClass, 
		   source, 
		   new String[] {
			 "motsReserves", "\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\'"
		   });	
		addAnnotation
		  (complexeEClass, 
		   source, 
		   new String[] {
			 "uniciteNums", "\n\t\t\tself.connecteur -> forAll(c1,c2 : Connecteur | c1.num = c2.num implies c1 = c2) "
		   });	
		addAnnotation
		  (portEClass, 
		   source, 
		   new String[] {
			 "motsReserves", "\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\'"
		   });	
		addAnnotation
		  (connecteurEClass, 
		   source, 
		   new String[] {
			 "numPositif", "\n\t\t\tself.num >= 0 "
		   });
	}

} //AdlPackageImpl
