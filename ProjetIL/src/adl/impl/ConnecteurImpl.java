/**
 */
package adl.impl;

import adl.AdlPackage;
import adl.Connecteur;

import adl.PortEntrée;
import adl.PortSortie;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link adl.impl.ConnecteurImpl#getNum <em>Num</em>}</li>
 *   <li>{@link adl.impl.ConnecteurImpl#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link adl.impl.ConnecteurImpl#getPortentrée <em>Portentrée</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnecteurImpl extends MinimalEObjectImpl.Container implements Connecteur {
	/**
	 * The default value of the '{@link #getNum() <em>Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNum() <em>Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum()
	 * @generated
	 * @ordered
	 */
	protected int num = NUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortsortie() <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortsortie()
	 * @generated
	 * @ordered
	 */
	protected PortSortie portsortie;

	/**
	 * The cached value of the '{@link #getPortentrée() <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortentrée()
	 * @generated
	 * @ordered
	 */
	protected PortEntrée portentrée;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnecteurImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdlPackage.Literals.CONNECTEUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNum() {
		return num;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNum(int newNum) {
		int oldNum = num;
		num = newNum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.CONNECTEUR__NUM, oldNum, num));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie getPortsortie() {
		if (portsortie != null && portsortie.eIsProxy()) {
			InternalEObject oldPortsortie = (InternalEObject)portsortie;
			portsortie = (PortSortie)eResolveProxy(oldPortsortie);
			if (portsortie != oldPortsortie) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.CONNECTEUR__PORTSORTIE, oldPortsortie, portsortie));
			}
		}
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie basicGetPortsortie() {
		return portsortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortsortie(PortSortie newPortsortie, NotificationChain msgs) {
		PortSortie oldPortsortie = portsortie;
		portsortie = newPortsortie;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.CONNECTEUR__PORTSORTIE, oldPortsortie, newPortsortie);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortsortie(PortSortie newPortsortie) {
		if (newPortsortie != portsortie) {
			NotificationChain msgs = null;
			if (portsortie != null)
				msgs = ((InternalEObject)portsortie).eInverseRemove(this, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, PortSortie.class, msgs);
			if (newPortsortie != null)
				msgs = ((InternalEObject)newPortsortie).eInverseAdd(this, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, PortSortie.class, msgs);
			msgs = basicSetPortsortie(newPortsortie, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.CONNECTEUR__PORTSORTIE, newPortsortie, newPortsortie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée getPortentrée() {
		if (portentrée != null && portentrée.eIsProxy()) {
			InternalEObject oldPortentrée = (InternalEObject)portentrée;
			portentrée = (PortEntrée)eResolveProxy(oldPortentrée);
			if (portentrée != oldPortentrée) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdlPackage.CONNECTEUR__PORTENTRÉE, oldPortentrée, portentrée));
			}
		}
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée basicGetPortentrée() {
		return portentrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortentrée(PortEntrée newPortentrée, NotificationChain msgs) {
		PortEntrée oldPortentrée = portentrée;
		portentrée = newPortentrée;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdlPackage.CONNECTEUR__PORTENTRÉE, oldPortentrée, newPortentrée);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortentrée(PortEntrée newPortentrée) {
		if (newPortentrée != portentrée) {
			NotificationChain msgs = null;
			if (portentrée != null)
				msgs = ((InternalEObject)portentrée).eInverseRemove(this, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, PortEntrée.class, msgs);
			if (newPortentrée != null)
				msgs = ((InternalEObject)newPortentrée).eInverseAdd(this, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, PortEntrée.class, msgs);
			msgs = basicSetPortentrée(newPortentrée, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdlPackage.CONNECTEUR__PORTENTRÉE, newPortentrée, newPortentrée));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				if (portsortie != null)
					msgs = ((InternalEObject)portsortie).eInverseRemove(this, AdlPackage.PORT_SORTIE__CONNECTEURSORTIE, PortSortie.class, msgs);
				return basicSetPortsortie((PortSortie)otherEnd, msgs);
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				if (portentrée != null)
					msgs = ((InternalEObject)portentrée).eInverseRemove(this, AdlPackage.PORT_ENTRÉE__CONNECTEURENTRÉE, PortEntrée.class, msgs);
				return basicSetPortentrée((PortEntrée)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				return basicSetPortsortie(null, msgs);
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				return basicSetPortentrée(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__NUM:
				return getNum();
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				if (resolve) return getPortsortie();
				return basicGetPortsortie();
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				if (resolve) return getPortentrée();
				return basicGetPortentrée();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__NUM:
				setNum((Integer)newValue);
				return;
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				setPortsortie((PortSortie)newValue);
				return;
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				setPortentrée((PortEntrée)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__NUM:
				setNum(NUM_EDEFAULT);
				return;
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				setPortsortie((PortSortie)null);
				return;
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				setPortentrée((PortEntrée)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdlPackage.CONNECTEUR__NUM:
				return num != NUM_EDEFAULT;
			case AdlPackage.CONNECTEUR__PORTSORTIE:
				return portsortie != null;
			case AdlPackage.CONNECTEUR__PORTENTRÉE:
				return portentrée != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (num: ");
		result.append(num);
		result.append(')');
		return result.toString();
	}

} //ConnecteurImpl
