/**
 */
package adl.impl;

import adl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdlFactoryImpl extends EFactoryImpl implements AdlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AdlFactory init() {
		try {
			AdlFactory theAdlFactory = (AdlFactory)EPackage.Registry.INSTANCE.getEFactory(AdlPackage.eNS_URI);
			if (theAdlFactory != null) {
				return theAdlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AdlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AdlPackage.PARTIE_APPLICATION: return createPartieApplication();
			case AdlPackage.PORT_ENTRÉE: return createPortEntrée();
			case AdlPackage.PORT_SORTIE: return createPortSortie();
			case AdlPackage.RESSOURCE: return createRessource();
			case AdlPackage.SIMPLE: return createSimple();
			case AdlPackage.COMPLEXE: return createComplexe();
			case AdlPackage.CONNECTEUR: return createConnecteur();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartieApplication createPartieApplication() {
		PartieApplicationImpl partieApplication = new PartieApplicationImpl();
		return partieApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEntrée createPortEntrée() {
		PortEntréeImpl portEntrée = new PortEntréeImpl();
		return portEntrée;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSortie createPortSortie() {
		PortSortieImpl portSortie = new PortSortieImpl();
		return portSortie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ressource createRessource() {
		RessourceImpl ressource = new RessourceImpl();
		return ressource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple createSimple() {
		SimpleImpl simple = new SimpleImpl();
		return simple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Complexe createComplexe() {
		ComplexeImpl complexe = new ComplexeImpl();
		return complexe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur createConnecteur() {
		ConnecteurImpl connecteur = new ConnecteurImpl();
		return connecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdlPackage getAdlPackage() {
		return (AdlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AdlPackage getPackage() {
		return AdlPackage.eINSTANCE;
	}

} //AdlFactoryImpl
