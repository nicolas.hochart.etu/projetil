/**
 */
package adl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Partie Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.PartieApplication#getNom <em>Nom</em>}</li>
 *   <li>{@link adl.PartieApplication#getPort <em>Port</em>}</li>
 *   <li>{@link adl.PartieApplication#getRessource <em>Ressource</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getPartieApplication()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniciteNoms motsReserves'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniciteNoms='\n\t\t\tself.port -> forAll(p1,p2 : Port | p1.nom = p2.nom implies p1 = p2) ' motsReserves='\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' and self.nom <> \'Port\''"
 * @generated
 */
public interface PartieApplication extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see adl.AdlPackage#getPartieApplication_Nom()
	 * @model required="true"
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link adl.PartieApplication#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference list.
	 * The list contents are of type {@link adl.Port}.
	 * It is bidirectional and its opposite is '{@link adl.Port#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference list.
	 * @see adl.AdlPackage#getPartieApplication_Port()
	 * @see adl.Port#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Port> getPort();

	/**
	 * Returns the value of the '<em><b>Ressource</b></em>' reference list.
	 * The list contents are of type {@link adl.Ressource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ressource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ressource</em>' reference list.
	 * @see adl.AdlPackage#getPartieApplication_Ressource()
	 * @model
	 * @generated
	 */
	EList<Ressource> getRessource();

} // PartieApplication
