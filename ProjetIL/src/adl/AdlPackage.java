/**
 */
package adl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see adl.AdlFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface AdlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "adl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///adl.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "adl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdlPackage eINSTANCE = adl.impl.AdlPackageImpl.init();

	/**
	 * The meta object id for the '{@link adl.impl.ApplicationLogicielleImpl <em>Application Logicielle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.ApplicationLogicielleImpl
	 * @see adl.impl.AdlPackageImpl#getApplicationLogicielle()
	 * @generated
	 */
	int APPLICATION_LOGICIELLE = 0;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__NOM = 0;

	/**
	 * The feature id for the '<em><b>Ressource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE__RESSOURCE = 1;

	/**
	 * The number of structural features of the '<em>Application Logicielle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Application Logicielle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_LOGICIELLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link adl.impl.PartieApplicationImpl <em>Partie Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.PartieApplicationImpl
	 * @see adl.impl.AdlPackageImpl#getPartieApplication()
	 * @generated
	 */
	int PARTIE_APPLICATION = 1;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__NOM = 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__PORT = 1;

	/**
	 * The feature id for the '<em><b>Ressource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION__RESSOURCE = 2;

	/**
	 * The number of structural features of the '<em>Partie Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Partie Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTIE_APPLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link adl.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.PortImpl
	 * @see adl.impl.AdlPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 7;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__OWNER = 0;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NOM = 1;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link adl.impl.PortEntréeImpl <em>Port Entrée</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.PortEntréeImpl
	 * @see adl.impl.AdlPackageImpl#getPortEntrée()
	 * @generated
	 */
	int PORT_ENTRÉE = 2;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE__OWNER = PORT__OWNER;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE__NOM = PORT__NOM;

	/**
	 * The feature id for the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE__PORTSORTIE = PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connecteurentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE__CONNECTEURENTRÉE = PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Port Entrée</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE_FEATURE_COUNT = PORT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Port Entrée</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_ENTRÉE_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link adl.impl.PortSortieImpl <em>Port Sortie</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.PortSortieImpl
	 * @see adl.impl.AdlPackageImpl#getPortSortie()
	 * @generated
	 */
	int PORT_SORTIE = 3;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE__OWNER = PORT__OWNER;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE__NOM = PORT__NOM;

	/**
	 * The feature id for the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE__PORTENTRÉE = PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connecteursortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE__CONNECTEURSORTIE = PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Port Sortie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE_FEATURE_COUNT = PORT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Port Sortie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SORTIE_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link adl.impl.RessourceImpl <em>Ressource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.RessourceImpl
	 * @see adl.impl.AdlPackageImpl#getRessource()
	 * @generated
	 */
	int RESSOURCE = 4;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE__NOM = 0;

	/**
	 * The number of structural features of the '<em>Ressource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Ressource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESSOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link adl.impl.SimpleImpl <em>Simple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.SimpleImpl
	 * @see adl.impl.AdlPackageImpl#getSimple()
	 * @generated
	 */
	int SIMPLE = 5;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE__NOM = APPLICATION_LOGICIELLE__NOM;

	/**
	 * The feature id for the '<em><b>Ressource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE__RESSOURCE = APPLICATION_LOGICIELLE__RESSOURCE;

	/**
	 * The feature id for the '<em><b>Simple ressource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE__SIMPLE_RESSOURCE = APPLICATION_LOGICIELLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FEATURE_COUNT = APPLICATION_LOGICIELLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION_COUNT = APPLICATION_LOGICIELLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link adl.impl.ComplexeImpl <em>Complexe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.ComplexeImpl
	 * @see adl.impl.AdlPackageImpl#getComplexe()
	 * @generated
	 */
	int COMPLEXE = 6;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__NOM = APPLICATION_LOGICIELLE__NOM;

	/**
	 * The feature id for the '<em><b>Ressource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__RESSOURCE = APPLICATION_LOGICIELLE__RESSOURCE;

	/**
	 * The feature id for the '<em><b>Partieapplication</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__PARTIEAPPLICATION = APPLICATION_LOGICIELLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Complexe</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__COMPLEXE = APPLICATION_LOGICIELLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Connecteur</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE__CONNECTEUR = APPLICATION_LOGICIELLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Complexe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE_FEATURE_COUNT = APPLICATION_LOGICIELLE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Complexe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEXE_OPERATION_COUNT = APPLICATION_LOGICIELLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link adl.impl.ConnecteurImpl <em>Connecteur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see adl.impl.ConnecteurImpl
	 * @see adl.impl.AdlPackageImpl#getConnecteur()
	 * @generated
	 */
	int CONNECTEUR = 8;

	/**
	 * The feature id for the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__NUM = 0;

	/**
	 * The feature id for the '<em><b>Portsortie</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__PORTSORTIE = 1;

	/**
	 * The feature id for the '<em><b>Portentrée</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR__PORTENTRÉE = 2;

	/**
	 * The number of structural features of the '<em>Connecteur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Connecteur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link adl.ApplicationLogicielle <em>Application Logicielle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application Logicielle</em>'.
	 * @see adl.ApplicationLogicielle
	 * @generated
	 */
	EClass getApplicationLogicielle();

	/**
	 * Returns the meta object for the attribute '{@link adl.ApplicationLogicielle#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see adl.ApplicationLogicielle#getNom()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EAttribute getApplicationLogicielle_Nom();

	/**
	 * Returns the meta object for the containment reference list '{@link adl.ApplicationLogicielle#getRessource <em>Ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ressource</em>'.
	 * @see adl.ApplicationLogicielle#getRessource()
	 * @see #getApplicationLogicielle()
	 * @generated
	 */
	EReference getApplicationLogicielle_Ressource();

	/**
	 * Returns the meta object for class '{@link adl.PartieApplication <em>Partie Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Partie Application</em>'.
	 * @see adl.PartieApplication
	 * @generated
	 */
	EClass getPartieApplication();

	/**
	 * Returns the meta object for the attribute '{@link adl.PartieApplication#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see adl.PartieApplication#getNom()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EAttribute getPartieApplication_Nom();

	/**
	 * Returns the meta object for the containment reference list '{@link adl.PartieApplication#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port</em>'.
	 * @see adl.PartieApplication#getPort()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EReference getPartieApplication_Port();

	/**
	 * Returns the meta object for the reference list '{@link adl.PartieApplication#getRessource <em>Ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ressource</em>'.
	 * @see adl.PartieApplication#getRessource()
	 * @see #getPartieApplication()
	 * @generated
	 */
	EReference getPartieApplication_Ressource();

	/**
	 * Returns the meta object for class '{@link adl.PortEntrée <em>Port Entrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Entrée</em>'.
	 * @see adl.PortEntrée
	 * @generated
	 */
	EClass getPortEntrée();

	/**
	 * Returns the meta object for the reference '{@link adl.PortEntrée#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portsortie</em>'.
	 * @see adl.PortEntrée#getPortsortie()
	 * @see #getPortEntrée()
	 * @generated
	 */
	EReference getPortEntrée_Portsortie();

	/**
	 * Returns the meta object for the reference '{@link adl.PortEntrée#getConnecteurentrée <em>Connecteurentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connecteurentrée</em>'.
	 * @see adl.PortEntrée#getConnecteurentrée()
	 * @see #getPortEntrée()
	 * @generated
	 */
	EReference getPortEntrée_Connecteurentrée();

	/**
	 * Returns the meta object for class '{@link adl.PortSortie <em>Port Sortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Sortie</em>'.
	 * @see adl.PortSortie
	 * @generated
	 */
	EClass getPortSortie();

	/**
	 * Returns the meta object for the reference '{@link adl.PortSortie#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portentrée</em>'.
	 * @see adl.PortSortie#getPortentrée()
	 * @see #getPortSortie()
	 * @generated
	 */
	EReference getPortSortie_Portentrée();

	/**
	 * Returns the meta object for the reference '{@link adl.PortSortie#getConnecteursortie <em>Connecteursortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connecteursortie</em>'.
	 * @see adl.PortSortie#getConnecteursortie()
	 * @see #getPortSortie()
	 * @generated
	 */
	EReference getPortSortie_Connecteursortie();

	/**
	 * Returns the meta object for class '{@link adl.Ressource <em>Ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ressource</em>'.
	 * @see adl.Ressource
	 * @generated
	 */
	EClass getRessource();

	/**
	 * Returns the meta object for the attribute '{@link adl.Ressource#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see adl.Ressource#getNom()
	 * @see #getRessource()
	 * @generated
	 */
	EAttribute getRessource_Nom();

	/**
	 * Returns the meta object for class '{@link adl.Simple <em>Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple</em>'.
	 * @see adl.Simple
	 * @generated
	 */
	EClass getSimple();

	/**
	 * Returns the meta object for the reference list '{@link adl.Simple#getSimple_ressource <em>Simple ressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Simple ressource</em>'.
	 * @see adl.Simple#getSimple_ressource()
	 * @see #getSimple()
	 * @generated
	 */
	EReference getSimple_Simple_ressource();

	/**
	 * Returns the meta object for class '{@link adl.Complexe <em>Complexe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complexe</em>'.
	 * @see adl.Complexe
	 * @generated
	 */
	EClass getComplexe();

	/**
	 * Returns the meta object for the containment reference list '{@link adl.Complexe#getPartieapplication <em>Partieapplication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Partieapplication</em>'.
	 * @see adl.Complexe#getPartieapplication()
	 * @see #getComplexe()
	 * @generated
	 */
	EReference getComplexe_Partieapplication();

	/**
	 * Returns the meta object for the containment reference list '{@link adl.Complexe#getComplexe <em>Complexe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Complexe</em>'.
	 * @see adl.Complexe#getComplexe()
	 * @see #getComplexe()
	 * @generated
	 */
	EReference getComplexe_Complexe();

	/**
	 * Returns the meta object for the containment reference list '{@link adl.Complexe#getConnecteur <em>Connecteur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connecteur</em>'.
	 * @see adl.Complexe#getConnecteur()
	 * @see #getComplexe()
	 * @generated
	 */
	EReference getComplexe_Connecteur();

	/**
	 * Returns the meta object for class '{@link adl.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see adl.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the container reference '{@link adl.Port#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see adl.Port#getOwner()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_Owner();

	/**
	 * Returns the meta object for the attribute '{@link adl.Port#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see adl.Port#getNom()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Nom();

	/**
	 * Returns the meta object for class '{@link adl.Connecteur <em>Connecteur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur</em>'.
	 * @see adl.Connecteur
	 * @generated
	 */
	EClass getConnecteur();

	/**
	 * Returns the meta object for the attribute '{@link adl.Connecteur#getNum <em>Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num</em>'.
	 * @see adl.Connecteur#getNum()
	 * @see #getConnecteur()
	 * @generated
	 */
	EAttribute getConnecteur_Num();

	/**
	 * Returns the meta object for the reference '{@link adl.Connecteur#getPortsortie <em>Portsortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portsortie</em>'.
	 * @see adl.Connecteur#getPortsortie()
	 * @see #getConnecteur()
	 * @generated
	 */
	EReference getConnecteur_Portsortie();

	/**
	 * Returns the meta object for the reference '{@link adl.Connecteur#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Portentrée</em>'.
	 * @see adl.Connecteur#getPortentrée()
	 * @see #getConnecteur()
	 * @generated
	 */
	EReference getConnecteur_Portentrée();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdlFactory getAdlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link adl.impl.ApplicationLogicielleImpl <em>Application Logicielle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.ApplicationLogicielleImpl
		 * @see adl.impl.AdlPackageImpl#getApplicationLogicielle()
		 * @generated
		 */
		EClass APPLICATION_LOGICIELLE = eINSTANCE.getApplicationLogicielle();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION_LOGICIELLE__NOM = eINSTANCE.getApplicationLogicielle_Nom();

		/**
		 * The meta object literal for the '<em><b>Ressource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_LOGICIELLE__RESSOURCE = eINSTANCE.getApplicationLogicielle_Ressource();

		/**
		 * The meta object literal for the '{@link adl.impl.PartieApplicationImpl <em>Partie Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.PartieApplicationImpl
		 * @see adl.impl.AdlPackageImpl#getPartieApplication()
		 * @generated
		 */
		EClass PARTIE_APPLICATION = eINSTANCE.getPartieApplication();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARTIE_APPLICATION__NOM = eINSTANCE.getPartieApplication_Nom();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTIE_APPLICATION__PORT = eINSTANCE.getPartieApplication_Port();

		/**
		 * The meta object literal for the '<em><b>Ressource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTIE_APPLICATION__RESSOURCE = eINSTANCE.getPartieApplication_Ressource();

		/**
		 * The meta object literal for the '{@link adl.impl.PortEntréeImpl <em>Port Entrée</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.PortEntréeImpl
		 * @see adl.impl.AdlPackageImpl#getPortEntrée()
		 * @generated
		 */
		EClass PORT_ENTRÉE = eINSTANCE.getPortEntrée();

		/**
		 * The meta object literal for the '<em><b>Portsortie</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_ENTRÉE__PORTSORTIE = eINSTANCE.getPortEntrée_Portsortie();

		/**
		 * The meta object literal for the '<em><b>Connecteurentrée</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_ENTRÉE__CONNECTEURENTRÉE = eINSTANCE.getPortEntrée_Connecteurentrée();

		/**
		 * The meta object literal for the '{@link adl.impl.PortSortieImpl <em>Port Sortie</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.PortSortieImpl
		 * @see adl.impl.AdlPackageImpl#getPortSortie()
		 * @generated
		 */
		EClass PORT_SORTIE = eINSTANCE.getPortSortie();

		/**
		 * The meta object literal for the '<em><b>Portentrée</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_SORTIE__PORTENTRÉE = eINSTANCE.getPortSortie_Portentrée();

		/**
		 * The meta object literal for the '<em><b>Connecteursortie</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_SORTIE__CONNECTEURSORTIE = eINSTANCE.getPortSortie_Connecteursortie();

		/**
		 * The meta object literal for the '{@link adl.impl.RessourceImpl <em>Ressource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.RessourceImpl
		 * @see adl.impl.AdlPackageImpl#getRessource()
		 * @generated
		 */
		EClass RESSOURCE = eINSTANCE.getRessource();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESSOURCE__NOM = eINSTANCE.getRessource_Nom();

		/**
		 * The meta object literal for the '{@link adl.impl.SimpleImpl <em>Simple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.SimpleImpl
		 * @see adl.impl.AdlPackageImpl#getSimple()
		 * @generated
		 */
		EClass SIMPLE = eINSTANCE.getSimple();

		/**
		 * The meta object literal for the '<em><b>Simple ressource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE__SIMPLE_RESSOURCE = eINSTANCE.getSimple_Simple_ressource();

		/**
		 * The meta object literal for the '{@link adl.impl.ComplexeImpl <em>Complexe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.ComplexeImpl
		 * @see adl.impl.AdlPackageImpl#getComplexe()
		 * @generated
		 */
		EClass COMPLEXE = eINSTANCE.getComplexe();

		/**
		 * The meta object literal for the '<em><b>Partieapplication</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEXE__PARTIEAPPLICATION = eINSTANCE.getComplexe_Partieapplication();

		/**
		 * The meta object literal for the '<em><b>Complexe</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEXE__COMPLEXE = eINSTANCE.getComplexe_Complexe();

		/**
		 * The meta object literal for the '<em><b>Connecteur</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEXE__CONNECTEUR = eINSTANCE.getComplexe_Connecteur();

		/**
		 * The meta object literal for the '{@link adl.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.PortImpl
		 * @see adl.impl.AdlPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__OWNER = eINSTANCE.getPort_Owner();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__NOM = eINSTANCE.getPort_Nom();

		/**
		 * The meta object literal for the '{@link adl.impl.ConnecteurImpl <em>Connecteur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see adl.impl.ConnecteurImpl
		 * @see adl.impl.AdlPackageImpl#getConnecteur()
		 * @generated
		 */
		EClass CONNECTEUR = eINSTANCE.getConnecteur();

		/**
		 * The meta object literal for the '<em><b>Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTEUR__NUM = eINSTANCE.getConnecteur_Num();

		/**
		 * The meta object literal for the '<em><b>Portsortie</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR__PORTSORTIE = eINSTANCE.getConnecteur_Portsortie();

		/**
		 * The meta object literal for the '<em><b>Portentrée</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR__PORTENTRÉE = eINSTANCE.getConnecteur_Portentrée();

	}

} //AdlPackage
