/**
 */
package adl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.Connecteur#getNum <em>Num</em>}</li>
 *   <li>{@link adl.Connecteur#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link adl.Connecteur#getPortentrée <em>Portentrée</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getConnecteur()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='numPositif'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot numPositif='\n\t\t\tself.num >= 0 '"
 * @generated
 */
public interface Connecteur extends EObject {
	/**
	 * Returns the value of the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num</em>' attribute.
	 * @see #setNum(int)
	 * @see adl.AdlPackage#getConnecteur_Num()
	 * @model required="true"
	 * @generated
	 */
	int getNum();

	/**
	 * Sets the value of the '{@link adl.Connecteur#getNum <em>Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num</em>' attribute.
	 * @see #getNum()
	 * @generated
	 */
	void setNum(int value);

	/**
	 * Returns the value of the '<em><b>Portsortie</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.PortSortie#getConnecteursortie <em>Connecteursortie</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portsortie</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portsortie</em>' reference.
	 * @see #setPortsortie(PortSortie)
	 * @see adl.AdlPackage#getConnecteur_Portsortie()
	 * @see adl.PortSortie#getConnecteursortie
	 * @model opposite="connecteursortie" required="true"
	 * @generated
	 */
	PortSortie getPortsortie();

	/**
	 * Sets the value of the '{@link adl.Connecteur#getPortsortie <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portsortie</em>' reference.
	 * @see #getPortsortie()
	 * @generated
	 */
	void setPortsortie(PortSortie value);

	/**
	 * Returns the value of the '<em><b>Portentrée</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.PortEntrée#getConnecteurentrée <em>Connecteurentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portentrée</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portentrée</em>' reference.
	 * @see #setPortentrée(PortEntrée)
	 * @see adl.AdlPackage#getConnecteur_Portentrée()
	 * @see adl.PortEntrée#getConnecteurentrée
	 * @model opposite="connecteurentrée" required="true"
	 * @generated
	 */
	PortEntrée getPortentrée();

	/**
	 * Sets the value of the '{@link adl.Connecteur#getPortentrée <em>Portentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portentrée</em>' reference.
	 * @see #getPortentrée()
	 * @generated
	 */
	void setPortentrée(PortEntrée value);

} // Connecteur
