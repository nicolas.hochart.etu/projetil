/**
 */
package adl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complexe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.Complexe#getPartieapplication <em>Partieapplication</em>}</li>
 *   <li>{@link adl.Complexe#getComplexe <em>Complexe</em>}</li>
 *   <li>{@link adl.Complexe#getConnecteur <em>Connecteur</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getComplexe()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniciteNums'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniciteNums='\n\t\t\tself.connecteur -> forAll(c1,c2 : Connecteur | c1.num = c2.num implies c1 = c2) '"
 * @generated
 */
public interface Complexe extends ApplicationLogicielle {
	/**
	 * Returns the value of the '<em><b>Partieapplication</b></em>' containment reference list.
	 * The list contents are of type {@link adl.PartieApplication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partieapplication</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partieapplication</em>' containment reference list.
	 * @see adl.AdlPackage#getComplexe_Partieapplication()
	 * @model containment="true"
	 * @generated
	 */
	EList<PartieApplication> getPartieapplication();

	/**
	 * Returns the value of the '<em><b>Complexe</b></em>' containment reference list.
	 * The list contents are of type {@link adl.Complexe}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complexe</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complexe</em>' containment reference list.
	 * @see adl.AdlPackage#getComplexe_Complexe()
	 * @model containment="true"
	 * @generated
	 */
	EList<Complexe> getComplexe();

	/**
	 * Returns the value of the '<em><b>Connecteur</b></em>' containment reference list.
	 * The list contents are of type {@link adl.Connecteur}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteur</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteur</em>' containment reference list.
	 * @see adl.AdlPackage#getComplexe_Connecteur()
	 * @model containment="true"
	 * @generated
	 */
	EList<Connecteur> getConnecteur();

} // Complexe
