/**
 */
package adl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Logicielle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.ApplicationLogicielle#getNom <em>Nom</em>}</li>
 *   <li>{@link adl.ApplicationLogicielle#getRessource <em>Ressource</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getApplicationLogicielle()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniciteNoms motsReserves'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniciteNoms='\n\t\t\tself.ressource -> forAll(r1,r2 : Ressource | r1.nom = r2.nom implies r1 = r2) ' motsReserves='\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' and self.nom <> \'Port\''"
 * @generated
 */
public interface ApplicationLogicielle extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see adl.AdlPackage#getApplicationLogicielle_Nom()
	 * @model required="true"
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link adl.ApplicationLogicielle#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Ressource</b></em>' containment reference list.
	 * The list contents are of type {@link adl.Ressource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ressource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ressource</em>' containment reference list.
	 * @see adl.AdlPackage#getApplicationLogicielle_Ressource()
	 * @model containment="true"
	 * @generated
	 */
	EList<Ressource> getRessource();

} // ApplicationLogicielle
