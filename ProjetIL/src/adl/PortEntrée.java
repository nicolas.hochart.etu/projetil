/**
 */
package adl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Entrée</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link adl.PortEntrée#getPortsortie <em>Portsortie</em>}</li>
 *   <li>{@link adl.PortEntrée#getConnecteurentrée <em>Connecteurentrée</em>}</li>
 * </ul>
 *
 * @see adl.AdlPackage#getPortEntrée()
 * @model
 * @generated
 */
public interface PortEntrée extends Port {
	/**
	 * Returns the value of the '<em><b>Portsortie</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.PortSortie#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portsortie</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portsortie</em>' reference.
	 * @see #setPortsortie(PortSortie)
	 * @see adl.AdlPackage#getPortEntrée_Portsortie()
	 * @see adl.PortSortie#getPortentrée
	 * @model opposite="portentrée" required="true"
	 * @generated
	 */
	PortSortie getPortsortie();

	/**
	 * Sets the value of the '{@link adl.PortEntrée#getPortsortie <em>Portsortie</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Portsortie</em>' reference.
	 * @see #getPortsortie()
	 * @generated
	 */
	void setPortsortie(PortSortie value);

	/**
	 * Returns the value of the '<em><b>Connecteurentrée</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link adl.Connecteur#getPortentrée <em>Portentrée</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteurentrée</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteurentrée</em>' reference.
	 * @see #setConnecteurentrée(Connecteur)
	 * @see adl.AdlPackage#getPortEntrée_Connecteurentrée()
	 * @see adl.Connecteur#getPortentrée
	 * @model opposite="portentrée" required="true"
	 * @generated
	 */
	Connecteur getConnecteurentrée();

	/**
	 * Sets the value of the '{@link adl.PortEntrée#getConnecteurentrée <em>Connecteurentrée</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connecteurentrée</em>' reference.
	 * @see #getConnecteurentrée()
	 * @generated
	 */
	void setConnecteurentrée(Connecteur value);

} // PortEntrée
