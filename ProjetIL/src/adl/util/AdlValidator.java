/**
 */
package adl.util;

import adl.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see adl.AdlPackage
 * @generated
 */
public class AdlValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final AdlValidator INSTANCE = new AdlValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "adl";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdlValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return AdlPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case AdlPackage.APPLICATION_LOGICIELLE:
				return validateApplicationLogicielle((ApplicationLogicielle)value, diagnostics, context);
			case AdlPackage.PARTIE_APPLICATION:
				return validatePartieApplication((PartieApplication)value, diagnostics, context);
			case AdlPackage.PORT_ENTRÉE:
				return validatePortEntrée((PortEntrée)value, diagnostics, context);
			case AdlPackage.PORT_SORTIE:
				return validatePortSortie((PortSortie)value, diagnostics, context);
			case AdlPackage.RESSOURCE:
				return validateRessource((Ressource)value, diagnostics, context);
			case AdlPackage.SIMPLE:
				return validateSimple((Simple)value, diagnostics, context);
			case AdlPackage.COMPLEXE:
				return validateComplexe((Complexe)value, diagnostics, context);
			case AdlPackage.PORT:
				return validatePort((Port)value, diagnostics, context);
			case AdlPackage.CONNECTEUR:
				return validateConnecteur((Connecteur)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationLogicielle(ApplicationLogicielle applicationLogicielle, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(applicationLogicielle, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_uniciteNoms(applicationLogicielle, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_motsReserves(applicationLogicielle, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniciteNoms constraint of '<em>Application Logicielle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String APPLICATION_LOGICIELLE__UNICITE_NOMS__EEXPRESSION = "\n" +
		"\t\t\tself.ressource -> forAll(r1,r2 : Ressource | r1.nom = r2.nom implies r1 = r2) ";

	/**
	 * Validates the uniciteNoms constraint of '<em>Application Logicielle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationLogicielle_uniciteNoms(ApplicationLogicielle applicationLogicielle, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.APPLICATION_LOGICIELLE,
				 applicationLogicielle,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniciteNoms",
				 APPLICATION_LOGICIELLE__UNICITE_NOMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the motsReserves constraint of '<em>Application Logicielle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String APPLICATION_LOGICIELLE__MOTS_RESERVES__EEXPRESSION = "\n" +
		"\t\t\tself.nom <> 'class' and self.nom <> 'package' and self.nom <> 'Port'";

	/**
	 * Validates the motsReserves constraint of '<em>Application Logicielle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationLogicielle_motsReserves(ApplicationLogicielle applicationLogicielle, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.APPLICATION_LOGICIELLE,
				 applicationLogicielle,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "motsReserves",
				 APPLICATION_LOGICIELLE__MOTS_RESERVES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePartieApplication(PartieApplication partieApplication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(partieApplication, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validatePartieApplication_uniciteNoms(partieApplication, diagnostics, context);
		if (result || diagnostics != null) result &= validatePartieApplication_motsReserves(partieApplication, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniciteNoms constraint of '<em>Partie Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PARTIE_APPLICATION__UNICITE_NOMS__EEXPRESSION = "\n" +
		"\t\t\tself.port -> forAll(p1,p2 : Port | p1.nom = p2.nom implies p1 = p2) ";

	/**
	 * Validates the uniciteNoms constraint of '<em>Partie Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePartieApplication_uniciteNoms(PartieApplication partieApplication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.PARTIE_APPLICATION,
				 partieApplication,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniciteNoms",
				 PARTIE_APPLICATION__UNICITE_NOMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the motsReserves constraint of '<em>Partie Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PARTIE_APPLICATION__MOTS_RESERVES__EEXPRESSION = "\n" +
		"\t\t\tself.nom <> 'class' and self.nom <> 'package' and self.nom <> 'Port'";

	/**
	 * Validates the motsReserves constraint of '<em>Partie Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePartieApplication_motsReserves(PartieApplication partieApplication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.PARTIE_APPLICATION,
				 partieApplication,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "motsReserves",
				 PARTIE_APPLICATION__MOTS_RESERVES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortEntrée(PortEntrée portEntrée, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(portEntrée, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portEntrée, diagnostics, context);
		if (result || diagnostics != null) result &= validatePort_motsReserves(portEntrée, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortSortie(PortSortie portSortie, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(portSortie, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portSortie, diagnostics, context);
		if (result || diagnostics != null) result &= validatePort_motsReserves(portSortie, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRessource(Ressource ressource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(ressource, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(ressource, diagnostics, context);
		if (result || diagnostics != null) result &= validateRessource_motsReserves(ressource, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the motsReserves constraint of '<em>Ressource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RESSOURCE__MOTS_RESERVES__EEXPRESSION = "\n" +
		"\t\t\tself.nom <> 'class' and self.nom <> 'package'";

	/**
	 * Validates the motsReserves constraint of '<em>Ressource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRessource_motsReserves(Ressource ressource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.RESSOURCE,
				 ressource,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "motsReserves",
				 RESSOURCE__MOTS_RESERVES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimple(Simple simple, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(simple, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_uniciteNoms(simple, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_motsReserves(simple, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexe(Complexe complexe, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(complexe, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_uniciteNoms(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationLogicielle_motsReserves(complexe, diagnostics, context);
		if (result || diagnostics != null) result &= validateComplexe_uniciteNums(complexe, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniciteNums constraint of '<em>Complexe</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPLEXE__UNICITE_NUMS__EEXPRESSION = "\n" +
		"\t\t\tself.connecteur -> forAll(c1,c2 : Connecteur | c1.num = c2.num implies c1 = c2) ";

	/**
	 * Validates the uniciteNums constraint of '<em>Complexe</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexe_uniciteNums(Complexe complexe, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.COMPLEXE,
				 complexe,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniciteNums",
				 COMPLEXE__UNICITE_NUMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePort(Port port, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(port, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(port, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(port, diagnostics, context);
		if (result || diagnostics != null) result &= validatePort_motsReserves(port, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the motsReserves constraint of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PORT__MOTS_RESERVES__EEXPRESSION = "\n" +
		"\t\t\tself.nom <> 'class' and self.nom <> 'package'";

	/**
	 * Validates the motsReserves constraint of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePort_motsReserves(Port port, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.PORT,
				 port,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "motsReserves",
				 PORT__MOTS_RESERVES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnecteur(Connecteur connecteur, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(connecteur, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(connecteur, diagnostics, context);
		if (result || diagnostics != null) result &= validateConnecteur_numPositif(connecteur, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the numPositif constraint of '<em>Connecteur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CONNECTEUR__NUM_POSITIF__EEXPRESSION = "\n" +
		"\t\t\tself.num >= 0 ";

	/**
	 * Validates the numPositif constraint of '<em>Connecteur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnecteur_numPositif(Connecteur connecteur, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(AdlPackage.Literals.CONNECTEUR,
				 connecteur,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "numPositif",
				 CONNECTEUR__NUM_POSITIF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //AdlValidator
