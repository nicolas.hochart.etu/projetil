package adl.prog;

import adl.*;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import java.util.Map ;
import java.util.Collections ;
import java.io.IOException ;

public class Main
{
  public static void main(String[] args)
  {
	Complexe videoclub = (Complexe) chargerModele("../../runtime-ProjetILConfiguration/testArchi/videoclubApp.adl", AdlPackage.eINSTANCE);
    Complexe questionnaire = (Complexe) chargerModele("../../runtime-ProjetILConfiguration/testArchi/questionnaire.adl", AdlPackage.eINSTANCE);
    //Vérification des connecteurs avec une fonction
    verifConnexions(videoclub);
    verifConnexions(questionnaire);
    //Sauvegarde des modèles
    sauvegardeModele("model/videoclubApp.adl", videoclub);
    sauvegardeModele("model/questionnaire.adl", questionnaire);
  }
  
  public static void verifConnexions(Complexe complexe)
  {
	/* Fonction qui vérifie si l'application complexe passée en paramètre a tout ses ports connectés. Une autre fonction récursive est appellée pour la vérification */
	System.out.print("Traitement de application complexe : " + complexe.getNom() + "\n");
	int erreur = 0;
	erreur = verifConnexionsRecursif(complexe);
	
	if (erreur == 0)
	{
	  System.out.print("Tous les ports sont connectés\n");
	}
	else
	{
	  System.out.print("Un ou plusieurs ports ne sont pas connectés\n");
	}
  }
  
  private static int verifConnexionsRecursif(Complexe complexe)
  {
	/*
	 Fonction récursive qui vérifie si l'application complexe passée en paramètre a tout ses ports de connectés,
	 et vérifie aussi (en s'appelant elle même) pour les sous-applications complexes de celle passée en paramètre. 
	*/
	int erreur = 0;
	
	//Vérification des connexions pour les sous-applications complexe de l'application complexe passée en paramètre
	EList<Complexe> listComplexes = complexe.getComplexe(); //Récupération de la liste des sous-applications complexe de l'application logicielle
	if (!listComplexes.isEmpty())
	{
	  for (Complexe complx : listComplexes) //Parcours des sous-applications complexe
	  {
		erreur = Math.max(verifConnexionsRecursif(complx),erreur);
		//Pour chaque sous-applications complexe, on vérifie si les ports sont correctement connectés.
		//Et de manière récursive, on vérifie ceci pour chaque sous-applications complexe du complexe, et ainsi de suite.
		//Si une application complexe, ou sous-application complexe, ou sous-(...)-sous-application complexe a un port déconnécté, alors "1" est retourné (erreur passe à 1)
	  }
	}
	
	//Vérification des connexions dans l'application complexe passée en paramètre
	//Il s'agit de l'application passée en paramètre, on ne regarde pas les sous-applications (déjà traitées au dessus).
	int i = 0;
	EList<PartieApplication> partiesApplicationList = complexe.getPartieapplication();
	for (PartieApplication partieApplication : partiesApplicationList) //Parcours des PartieApplications
	{
	  EList<Port> portsListe = partieApplication.getPort(); //Parcours des Ports de chaque PartieApplication
	  for (Port port : portsListe)
	  {
		if (port instanceof PortSortie) //Vérification des connexions (+affichage du message)
		{
		  i++;
		  Port portDest = getDest(port);
		  if (portDest != null)
		  {
			 System.out.print("Connecteur n°" + i + ": " + partieApplication.getNom() + "(" + getNomPort(port) + ")->" + portDest.getOwner().getNom() + "(" + getNomPort(portDest) + ")\n");
		  }
		  else
		  {
			 System.out.print("Attention, le port n°" + i + ": " + partieApplication.getNom() + "(" + getNomPort(port) + ") est déconnécté\n");
			 erreur = 1;
		  }
		}
      }
	}
	return erreur; //erreur est à 1 si une application complexe parmis les sous-applications OU l'application complexe passée en paramètre a un port non connecté.
  }
  
  private static String getNomPort(Port port)
  {
	/*
	 Retourne le nom du port passé en paramètre 
	 Cette fonction était utilisée quand nous n'avions pas encore d'attribut "nom" dans la classe abstraite "Port". Ce n'est plus le cas
	*/
	String nomPort = "";
	if (port instanceof PortEntrée)
	{
	  PortEntrée portEntree = (PortEntrée) port;
	  nomPort = portEntree.getNom();
	}
	else if (port instanceof PortSortie)
	{
	  PortSortie portSortie = (PortSortie) port;
	  nomPort = portSortie.getNom();
	}
	return nomPort;
  }
  
  private static Port getDest(Port port)
  {
	/* Retourne le port connecté au port passé en paramètre */
	if (port instanceof PortEntrée)
	{
	  return ((PortEntrée) port).getPortsortie();
	}
	else
	{
	  return ((PortSortie) port).getPortentrée();
	}
  }
  
  public static void sauvegardeModele(String cheminFichier,EObject p1)
  {
    URI fileURI = URI.createFileURI(cheminFichier);
    Resource resource = new XMIResourceFactoryImpl().createResource(fileURI);
    resource.getContents().add( p1 );
    try
    {
      resource.save(Collections.EMPTY_MAP);
    }
    catch (IOException e) { e.printStackTrace(); }
  }
  
  public static EObject chargerModele(String cheminFichier, EPackage typeModele)
  {
    typeModele.eClass();
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put(typeModele.getNsPrefix(), new XMIResourceFactoryImpl());
    ResourceSet resSet=new ResourceSetImpl();
    Resource res = resSet.getResource(URI.createURI(cheminFichier),true);
    return res.getContents().get(0);
  }
}
