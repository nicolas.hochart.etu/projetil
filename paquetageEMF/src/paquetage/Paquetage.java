/**
 */
package paquetage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paquetage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.Paquetage#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getPaquetage()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniciteNoms'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniciteNoms='\n\t\t\tself.elements -> forAll(e1, e2 : ElementModele | e1.nom = e2.nom implies e1 = e2)'"
 * @generated
 */
public interface Paquetage extends ElementModele {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link paquetage.ElementModele}.
	 * It is bidirectional and its opposite is '{@link paquetage.ElementModele#getProprietaire <em>Proprietaire</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see paquetage.PaquetagePackage#getPaquetage_Elements()
	 * @see paquetage.ElementModele#getProprietaire
	 * @model opposite="proprietaire" containment="true"
	 *        extendedMetaData="kind='element'"
	 * @generated
	 */
	EList<ElementModele> getElements();

} // Paquetage
