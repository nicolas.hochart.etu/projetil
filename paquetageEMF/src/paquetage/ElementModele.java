/**
 */
package paquetage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Modele</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.ElementModele#getNom <em>Nom</em>}</li>
 *   <li>{@link paquetage.ElementModele#getProprietaire <em>Proprietaire</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getElementModele()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='motsReserves'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot motsReserves='\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' '"
 * @generated
 */
public interface ElementModele extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see paquetage.PaquetagePackage#getElementModele_Nom()
	 * @model required="true"
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link paquetage.ElementModele#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Proprietaire</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link paquetage.Paquetage#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proprietaire</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proprietaire</em>' container reference.
	 * @see #setProprietaire(Paquetage)
	 * @see paquetage.PaquetagePackage#getElementModele_Proprietaire()
	 * @see paquetage.Paquetage#getElements
	 * @model opposite="elements" transient="false"
	 * @generated
	 */
	Paquetage getProprietaire();

	/**
	 * Sets the value of the '{@link paquetage.ElementModele#getProprietaire <em>Proprietaire</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proprietaire</em>' container reference.
	 * @see #getProprietaire()
	 * @generated
	 */
	void setProprietaire(Paquetage value);

} // ElementModele
