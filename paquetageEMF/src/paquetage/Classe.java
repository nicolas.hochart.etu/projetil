/**
 */
package paquetage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link paquetage.Classe#getAcces <em>Acces</em>}</li>
 * </ul>
 *
 * @see paquetage.PaquetagePackage#getClasse()
 * @model
 * @generated
 */
public interface Classe extends ElementModele {

	/**
	 * Returns the value of the '<em><b>Acces</b></em>' attribute.
	 * The literals are from the enumeration {@link paquetage.ModeAcces}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acces</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acces</em>' attribute.
	 * @see paquetage.ModeAcces
	 * @see #setAcces(ModeAcces)
	 * @see paquetage.PaquetagePackage#getClasse_Acces()
	 * @model required="true"
	 * @generated
	 */
	ModeAcces getAcces();

	/**
	 * Sets the value of the '{@link paquetage.Classe#getAcces <em>Acces</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acces</em>' attribute.
	 * @see paquetage.ModeAcces
	 * @see #getAcces()
	 * @generated
	 */
	void setAcces(ModeAcces value);
} // Classe
