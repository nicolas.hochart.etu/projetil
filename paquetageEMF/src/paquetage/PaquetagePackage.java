/**
 */
package paquetage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see paquetage.PaquetageFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface PaquetagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "paquetage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///paquetage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "paquetage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PaquetagePackage eINSTANCE = paquetage.impl.PaquetagePackageImpl.init();

	/**
	 * The meta object id for the '{@link paquetage.impl.ElementModeleImpl <em>Element Modele</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.ElementModeleImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getElementModele()
	 * @generated
	 */
	int ELEMENT_MODELE = 0;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_MODELE__NOM = 0;

	/**
	 * The feature id for the '<em><b>Proprietaire</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_MODELE__PROPRIETAIRE = 1;

	/**
	 * The number of structural features of the '<em>Element Modele</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_MODELE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Element Modele</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_MODELE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.ClasseImpl <em>Classe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.ClasseImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getClasse()
	 * @generated
	 */
	int CLASSE = 1;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSE__NOM = ELEMENT_MODELE__NOM;

	/**
	 * The feature id for the '<em><b>Proprietaire</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSE__PROPRIETAIRE = ELEMENT_MODELE__PROPRIETAIRE;

	/**
	 * The feature id for the '<em><b>Acces</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSE__ACCES = ELEMENT_MODELE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Classe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSE_FEATURE_COUNT = ELEMENT_MODELE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Classe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSE_OPERATION_COUNT = ELEMENT_MODELE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link paquetage.impl.PaquetageImpl <em>Paquetage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.impl.PaquetageImpl
	 * @see paquetage.impl.PaquetagePackageImpl#getPaquetage()
	 * @generated
	 */
	int PAQUETAGE = 2;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAQUETAGE__NOM = ELEMENT_MODELE__NOM;

	/**
	 * The feature id for the '<em><b>Proprietaire</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAQUETAGE__PROPRIETAIRE = ELEMENT_MODELE__PROPRIETAIRE;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAQUETAGE__ELEMENTS = ELEMENT_MODELE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Paquetage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAQUETAGE_FEATURE_COUNT = ELEMENT_MODELE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Paquetage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAQUETAGE_OPERATION_COUNT = ELEMENT_MODELE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link paquetage.ModeAcces <em>Mode Acces</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see paquetage.ModeAcces
	 * @see paquetage.impl.PaquetagePackageImpl#getModeAcces()
	 * @generated
	 */
	int MODE_ACCES = 3;


	/**
	 * Returns the meta object for class '{@link paquetage.ElementModele <em>Element Modele</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Modele</em>'.
	 * @see paquetage.ElementModele
	 * @generated
	 */
	EClass getElementModele();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.ElementModele#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see paquetage.ElementModele#getNom()
	 * @see #getElementModele()
	 * @generated
	 */
	EAttribute getElementModele_Nom();

	/**
	 * Returns the meta object for the container reference '{@link paquetage.ElementModele#getProprietaire <em>Proprietaire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Proprietaire</em>'.
	 * @see paquetage.ElementModele#getProprietaire()
	 * @see #getElementModele()
	 * @generated
	 */
	EReference getElementModele_Proprietaire();

	/**
	 * Returns the meta object for class '{@link paquetage.Classe <em>Classe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Classe</em>'.
	 * @see paquetage.Classe
	 * @generated
	 */
	EClass getClasse();

	/**
	 * Returns the meta object for the attribute '{@link paquetage.Classe#getAcces <em>Acces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Acces</em>'.
	 * @see paquetage.Classe#getAcces()
	 * @see #getClasse()
	 * @generated
	 */
	EAttribute getClasse_Acces();

	/**
	 * Returns the meta object for class '{@link paquetage.Paquetage <em>Paquetage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paquetage</em>'.
	 * @see paquetage.Paquetage
	 * @generated
	 */
	EClass getPaquetage();

	/**
	 * Returns the meta object for the containment reference list '{@link paquetage.Paquetage#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see paquetage.Paquetage#getElements()
	 * @see #getPaquetage()
	 * @generated
	 */
	EReference getPaquetage_Elements();

	/**
	 * Returns the meta object for enum '{@link paquetage.ModeAcces <em>Mode Acces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mode Acces</em>'.
	 * @see paquetage.ModeAcces
	 * @generated
	 */
	EEnum getModeAcces();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PaquetageFactory getPaquetageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link paquetage.impl.ElementModeleImpl <em>Element Modele</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.ElementModeleImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getElementModele()
		 * @generated
		 */
		EClass ELEMENT_MODELE = eINSTANCE.getElementModele();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT_MODELE__NOM = eINSTANCE.getElementModele_Nom();

		/**
		 * The meta object literal for the '<em><b>Proprietaire</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_MODELE__PROPRIETAIRE = eINSTANCE.getElementModele_Proprietaire();

		/**
		 * The meta object literal for the '{@link paquetage.impl.ClasseImpl <em>Classe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.ClasseImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getClasse()
		 * @generated
		 */
		EClass CLASSE = eINSTANCE.getClasse();

		/**
		 * The meta object literal for the '<em><b>Acces</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASSE__ACCES = eINSTANCE.getClasse_Acces();

		/**
		 * The meta object literal for the '{@link paquetage.impl.PaquetageImpl <em>Paquetage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.impl.PaquetageImpl
		 * @see paquetage.impl.PaquetagePackageImpl#getPaquetage()
		 * @generated
		 */
		EClass PAQUETAGE = eINSTANCE.getPaquetage();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAQUETAGE__ELEMENTS = eINSTANCE.getPaquetage_Elements();

		/**
		 * The meta object literal for the '{@link paquetage.ModeAcces <em>Mode Acces</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see paquetage.ModeAcces
		 * @see paquetage.impl.PaquetagePackageImpl#getModeAcces()
		 * @generated
		 */
		EEnum MODE_ACCES = eINSTANCE.getModeAcces();

	}

} //PaquetagePackage
