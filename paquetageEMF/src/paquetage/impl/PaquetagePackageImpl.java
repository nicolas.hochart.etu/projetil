/**
 */
package paquetage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import paquetage.Classe;
import paquetage.ElementModele;
import paquetage.ModeAcces;
import paquetage.Paquetage;
import paquetage.PaquetageFactory;
import paquetage.PaquetagePackage;
import paquetage.util.PaquetageValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PaquetagePackageImpl extends EPackageImpl implements PaquetagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementModeleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paquetageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modeAccesEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see paquetage.PaquetagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PaquetagePackageImpl() {
		super(eNS_URI, PaquetageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PaquetagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PaquetagePackage init() {
		if (isInited) return (PaquetagePackage)EPackage.Registry.INSTANCE.getEPackage(PaquetagePackage.eNS_URI);

		// Obtain or create and register package
		PaquetagePackageImpl thePaquetagePackage = (PaquetagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PaquetagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PaquetagePackageImpl());

		isInited = true;

		// Create package meta-data objects
		thePaquetagePackage.createPackageContents();

		// Initialize created meta-data
		thePaquetagePackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(thePaquetagePackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return PaquetageValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		thePaquetagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PaquetagePackage.eNS_URI, thePaquetagePackage);
		return thePaquetagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementModele() {
		return elementModeleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElementModele_Nom() {
		return (EAttribute)elementModeleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementModele_Proprietaire() {
		return (EReference)elementModeleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClasse() {
		return classeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClasse_Acces() {
		return (EAttribute)classeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaquetage() {
		return paquetageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPaquetage_Elements() {
		return (EReference)paquetageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getModeAcces() {
		return modeAccesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaquetageFactory getPaquetageFactory() {
		return (PaquetageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		elementModeleEClass = createEClass(ELEMENT_MODELE);
		createEAttribute(elementModeleEClass, ELEMENT_MODELE__NOM);
		createEReference(elementModeleEClass, ELEMENT_MODELE__PROPRIETAIRE);

		classeEClass = createEClass(CLASSE);
		createEAttribute(classeEClass, CLASSE__ACCES);

		paquetageEClass = createEClass(PAQUETAGE);
		createEReference(paquetageEClass, PAQUETAGE__ELEMENTS);

		// Create enums
		modeAccesEEnum = createEEnum(MODE_ACCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		classeEClass.getESuperTypes().add(this.getElementModele());
		paquetageEClass.getESuperTypes().add(this.getElementModele());

		// Initialize classes, features, and operations; add parameters
		initEClass(elementModeleEClass, ElementModele.class, "ElementModele", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getElementModele_Nom(), ecorePackage.getEString(), "nom", null, 1, 1, ElementModele.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementModele_Proprietaire(), this.getPaquetage(), this.getPaquetage_Elements(), "proprietaire", null, 0, 1, ElementModele.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classeEClass, Classe.class, "Classe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClasse_Acces(), this.getModeAcces(), "acces", null, 1, 1, Classe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(paquetageEClass, Paquetage.class, "Paquetage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPaquetage_Elements(), this.getElementModele(), this.getElementModele_Proprietaire(), "elements", null, 0, -1, Paquetage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(modeAccesEEnum, ModeAcces.class, "ModeAcces");
		addEEnumLiteral(modeAccesEEnum, ModeAcces.PUBLIQUE);
		addEEnumLiteral(modeAccesEEnum, ModeAcces.PRIVE);
		addEEnumLiteral(modeAccesEEnum, ModeAcces.PROTEGE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });	
		addAnnotation
		  (getPaquetage_Elements(), 
		   source, 
		   new String[] {
			 "kind", "element"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (elementModeleEClass, 
		   source, 
		   new String[] {
			 "constraints", "motsReserves"
		   });	
		addAnnotation
		  (paquetageEClass, 
		   source, 
		   new String[] {
			 "constraints", "uniciteNoms"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (elementModeleEClass, 
		   source, 
		   new String[] {
			 "motsReserves", "\n\t\t\tself.nom <> \'class\' and self.nom <> \'package\' "
		   });	
		addAnnotation
		  (paquetageEClass, 
		   source, 
		   new String[] {
			 "uniciteNoms", "\n\t\t\tself.elements -> forAll(e1, e2 : ElementModele | e1.nom = e2.nom implies e1 = e2)"
		   });
	}

} //PaquetagePackageImpl
