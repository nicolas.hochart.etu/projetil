/**
 */
package paquetage.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import paquetage.Classe;
import paquetage.ModeAcces;
import paquetage.PaquetagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link paquetage.impl.ClasseImpl#getAcces <em>Acces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClasseImpl extends ElementModeleImpl implements Classe {
	/**
	 * The default value of the '{@link #getAcces() <em>Acces</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcces()
	 * @generated
	 * @ordered
	 */
	protected static final ModeAcces ACCES_EDEFAULT = ModeAcces.PUBLIQUE;
	/**
	 * The cached value of the '{@link #getAcces() <em>Acces</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcces()
	 * @generated
	 * @ordered
	 */
	protected ModeAcces acces = ACCES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClasseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PaquetagePackage.Literals.CLASSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeAcces getAcces() {
		return acces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcces(ModeAcces newAcces) {
		ModeAcces oldAcces = acces;
		acces = newAcces == null ? ACCES_EDEFAULT : newAcces;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PaquetagePackage.CLASSE__ACCES, oldAcces, acces));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PaquetagePackage.CLASSE__ACCES:
				return getAcces();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PaquetagePackage.CLASSE__ACCES:
				setAcces((ModeAcces)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PaquetagePackage.CLASSE__ACCES:
				setAcces(ACCES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PaquetagePackage.CLASSE__ACCES:
				return acces != ACCES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (acces: ");
		result.append(acces);
		result.append(')');
		return result.toString();
	}

} //ClasseImpl
