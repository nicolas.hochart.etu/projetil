/**
 */
package paquetage.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import paquetage.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see paquetage.PaquetagePackage
 * @generated
 */
public class PaquetageValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final PaquetageValidator INSTANCE = new PaquetageValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "paquetage";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaquetageValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return PaquetagePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case PaquetagePackage.ELEMENT_MODELE:
				return validateElementModele((ElementModele)value, diagnostics, context);
			case PaquetagePackage.CLASSE:
				return validateClasse((Classe)value, diagnostics, context);
			case PaquetagePackage.PAQUETAGE:
				return validatePaquetage((Paquetage)value, diagnostics, context);
			case PaquetagePackage.MODE_ACCES:
				return validateModeAcces((ModeAcces)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementModele(ElementModele elementModele, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(elementModele, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(elementModele, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementModele_motsReserves(elementModele, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the motsReserves constraint of '<em>Element Modele</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ELEMENT_MODELE__MOTS_RESERVES__EEXPRESSION = "\n" +
		"\t\t\tself.nom <> 'class' and self.nom <> 'package' ";

	/**
	 * Validates the motsReserves constraint of '<em>Element Modele</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementModele_motsReserves(ElementModele elementModele, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PaquetagePackage.Literals.ELEMENT_MODELE,
				 elementModele,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "motsReserves",
				 ELEMENT_MODELE__MOTS_RESERVES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClasse(Classe classe, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(classe, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(classe, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementModele_motsReserves(classe, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaquetage(Paquetage paquetage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(paquetage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementModele_motsReserves(paquetage, diagnostics, context);
		if (result || diagnostics != null) result &= validatePaquetage_uniciteNoms(paquetage, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniciteNoms constraint of '<em>Paquetage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PAQUETAGE__UNICITE_NOMS__EEXPRESSION = "\n" +
		"\t\t\tself.elements -> forAll(e1, e2 : ElementModele | e1.nom = e2.nom implies e1 = e2)";

	/**
	 * Validates the uniciteNoms constraint of '<em>Paquetage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaquetage_uniciteNoms(Paquetage paquetage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PaquetagePackage.Literals.PAQUETAGE,
				 paquetage,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniciteNoms",
				 PAQUETAGE__UNICITE_NOMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModeAcces(ModeAcces modeAcces, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //PaquetageValidator
