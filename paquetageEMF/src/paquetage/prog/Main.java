package paquetage.prog ;

import paquetage.* ;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import java.util.Map ;
import java.util.Collections ;
import java.io.IOException ;

public class Main {
  public static void main(String[] args) {
    PaquetageFactory fabrique = PaquetageFactory.eINSTANCE ;
    Paquetage p1 = fabrique.createPaquetage() ; Paquetage p2 = fabrique.createPaquetage();
    Classe c1 = fabrique.createClasse(); Classe c2 = fabrique.createClasse();
    p1.setNom("P1") ; p2.setNom("P2") ; c1.setNom("C1") ; c2.setNom("C2") ;
    c1.setProprietaire(p1) ; c2.setProprietaire(p1) ;
    p2.setProprietaire(p1) ;
    for (ElementModele elt : p1.getElements()) System.out.println(elt.getNom()) ;
    sauvegardeModele("model/test2.paquetage", p1) ;	
    Paquetage modeleFromTP1 = (Paquetage)
      chargerModele("../../runtime-ProjetILConfiguration/testPaquetage/test2.paquetage",
                    PaquetagePackage.eINSTANCE);
    sauvegardeModele("model/test1.paquetage", modeleFromTP1) ;
  }
  public static void sauvegardeModele(String cheminFichier,EObject p1) {
    URI fileURI = URI.createFileURI(cheminFichier);
    Resource resource = new XMIResourceFactoryImpl().createResource(fileURI);
    resource.getContents().add( p1 );
    try {
      resource.save(Collections.EMPTY_MAP);
    } catch (IOException e) { e.printStackTrace(); }
  }
  public static EObject chargerModele(String cheminFichier, EPackage typeModele) {
    typeModele.eClass();
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put(typeModele.getNsPrefix(), new XMIResourceFactoryImpl());
    ResourceSet resSet=new ResourceSetImpl();
    Resource res = resSet.getResource(URI.createURI(cheminFichier),true);
    return res.getContents().get(0);
  }
}
